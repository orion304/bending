package main.java.org.valor.bending.main;

public class BendingMenu {

	Enum<?>[] choices = { null, null, null, null, null, null, null, null, null,
			null, null, null };

	public BendingMenu() {

	}

	public Enum<?> getChoice(int i) {
		return this.choices[i];
	}

	public void setChoice(int i, Enum<?> choice) {
		this.choices[i] = choice;
	}

}
