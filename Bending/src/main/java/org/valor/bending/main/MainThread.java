package main.java.org.valor.bending.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;

import main.java.org.valor.bending.abilities.AbilityType;
import main.java.org.valor.bending.abilities.Progression;
import main.java.org.valor.bending.abilities.earth.MovingRock;
import main.java.org.valor.bending.abilities.earth.StoredBlocks;
import main.java.org.valor.bending.abilities.water.Ice;
import net.minecraft.server.v1_12_R1.EnumParticle;
import src.main.java.org.orion304.utils.CraftMethods;

public class MainThread implements Runnable {

	private static final List<AbilityType> waterSources = new ArrayList<>();
	private static final Map<LivingEntity, LivingEntity> fliers = new HashMap<>();

	static {
		waterSources.add(AbilityType.WATER_BLAST);
		waterSources.add(AbilityType.WAVE);
	}

	public static void causeFlight(LivingEntity thrower, Entity flier) {
		if (flier instanceof LivingEntity) {
			fliers.put((LivingEntity) flier, thrower);
		}
	}

	public static LivingEntity getThrower(LivingEntity thrown) {
		return fliers.get(thrown);
	}

	private final Bending plugin;

	private int tick = 0;

	public MainThread(Bending instance) {
		this.plugin = instance;
	}

	@Override
	public void run() {
		this.tick++;
		for (BendingPlayer player : this.plugin.handler.getCustomPlayers()) {
			if (player.earthSource != null
					&& (player.getPlayer().getLocation()
							.distanceSquared(player.earthSource.getLocation()) > 64 || player
							.getAbility() != AbilityType.EARTH_BLAST)) {
				player.earthSource.drop();
				player.earthSource = null;
			}

			if (player.waterSource != null
					&& (player.getPlayer().getLocation()
							.distanceSquared(player.waterSource.getLocation()) > 225 || !waterSources
							.contains(player.getAbility()))) {
				player.waterSource = null;
			}

			if (player.earthSource != null) {
				player.sendPacket(CraftMethods.getParticlePacket(
						EnumParticle.CRIT, true, player.earthSource
								.getLocation().clone(), .3F, .3F, .3F, 0, 5));
				player.earthSource.handle();
			}

			if (player.waterSource != null) {
				player.sendPacket(CraftMethods.getParticlePacket(
						EnumParticle.SMOKE_NORMAL, true, player.waterSource
								.getLocation().add(.5, .5, .5), .2F, .2F, .2F,
						0, 5));
			}
		}

		Progression.handle();
		MovingRock.progressAll();
		StoredBlocks.handle(this.tick);
		Ice.handle();

		Set<LivingEntity> flyKeys = new HashSet<>();
		flyKeys.addAll(fliers.keySet());
		for (LivingEntity key : flyKeys) {
			if (key.isOnGround()) {
				fliers.remove(key);
			}
		}
	}

}
