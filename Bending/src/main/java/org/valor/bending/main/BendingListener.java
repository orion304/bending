package main.java.org.valor.bending.main;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;

import main.java.org.valor.bending.abilities.AbilityProjectile;
import main.java.org.valor.bending.abilities.AbilityType;
import main.java.org.valor.bending.abilities.Element;
import main.java.org.valor.bending.abilities.air.AirPunch;
import main.java.org.valor.bending.abilities.earth.EarthBlast;
import main.java.org.valor.bending.abilities.fire.FireBlast;
import main.java.org.valor.bending.abilities.water.Ice;
import main.java.org.valor.bending.abilities.water.WaterBlast;
import src.main.java.org.orion304.holographicmenu.HolographicMenuClickEvent;
import src.main.java.org.orion304.projectile.CustomProjectile;
import src.main.java.org.orion304.projectile.CustomProjectileHitBlockEvent;
import src.main.java.org.orion304.projectile.CustomProjectileHitEntityEvent;

public class BendingListener implements Listener {

	private final Bending plugin;

	public BendingListener(Bending instance) {
		this.plugin = instance;
	}

	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		Block block = event.getBlock();
		if (Ice.isIce(block)) {
			Ice.destroy(block);
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onBlockFade(BlockFadeEvent event) {
		if (Ice.isIce(event.getBlock())) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onCustomProjectileHitBlock(CustomProjectileHitBlockEvent event) {
		CustomProjectile projectile = event.getProjectile();
		if (projectile instanceof AbilityProjectile) {

		}
	}

	@EventHandler
	public void onCustomProjectileHitEntity(CustomProjectileHitEntityEvent event) {
		LivingEntity target = event.getClosestEntity();
		CustomProjectile projectile = event.getProjectile();
		LivingEntity shooter = projectile.getShooter();
		Location loc = target.getLocation();
		if (projectile instanceof WaterBlast) {
			if (Bending.isBendable(shooter, loc, AbilityType.WATER_BLAST)) {
				target.damage(3, shooter);
			} else {
				event.setCancelled(true);
			}
		} else if (projectile instanceof EarthBlast) {
			if (Bending.isBendable(shooter, loc, AbilityType.EARTH_BLAST)) {
				target.damage(4, shooter);
			} else {
				event.setCancelled(true);
			}
		} else if (projectile instanceof FireBlast) {
			if (Bending.isBendable(shooter, loc, AbilityType.FIRE_BLAST)) {
				target.damage(4, shooter);
			} else {
				event.setCancelled(true);
			}
		} else if (projectile instanceof AirPunch) {
			if (Bending.isBendable(shooter, loc, AbilityType.AIR_BURST)) {
				target.damage(1, shooter);
			} else {
				event.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onHolographicMenuClick(HolographicMenuClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		BendingPlayer bPlayer = this.plugin.handler.getCustomPlayer(player);
		bPlayer.clickMenu();
	}

	@EventHandler
	public void onPlayerDamage(EntityDamageEvent event) {
		Entity entity = event.getEntity();
		if (entity instanceof Player) {
			Player player = (Player) entity;
			BendingPlayer bPlayer = this.plugin.handler.getCustomPlayer(player);
			if (event.getCause() == DamageCause.FALL) {
				if (bPlayer.isBender(Element.AIR)
						&& player.hasPermission("bending.air.nofall")) {
					event.setCancelled(true);
				}
			}
		}

		if (!event.isCancelled() && event.getCause() == DamageCause.FALL) {
			LivingEntity lEntity;
			if (entity instanceof LivingEntity) {
				lEntity = (LivingEntity) entity;
				LivingEntity thrower = MainThread.getThrower(lEntity);
				if (thrower != null) {
					event.setCancelled(true);
					lEntity.damage(event.getDamage(), thrower);
				}
			}
		}
	}

	@EventHandler
	public void onPlayerKick(PlayerKickEvent event) {
		Player player = event.getPlayer();
		if (MainThread.getThrower(player) != null) {
			event.setCancelled(true);
			event.setLeaveMessage("");
			event.setReason("");
		}
	}

	@EventHandler
	public void onPlayerToggleSneak(PlayerToggleSneakEvent event) {
		Player player = event.getPlayer();
		BendingPlayer bPlayer = this.plugin.handler.getCustomPlayer(player);
		bPlayer.sneak(event.isSneaking());
	}

}
