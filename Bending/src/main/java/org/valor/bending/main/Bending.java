package main.java.org.valor.bending.main;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import main.java.org.valor.bending.abilities.AbilityType;
import main.java.org.valor.bending.abilities.Progression;
import main.java.org.valor.bending.abilities.earth.StoredBlocks;
import main.java.org.valor.bending.abilities.water.Ice;
import main.java.org.valor.bending.sounds.BendingSoundManager;
import net.minecraft.server.v1_12_R1.EnumParticle;
import src.main.java.org.orion304.OrionPlugin;
import src.main.java.org.orion304.SQLHandler;
import src.main.java.org.orion304.fakeentity.FakeBlock;
import src.main.java.org.orion304.player.CustomPlayerHandler;
import src.main.java.org.orion304.utils.CraftMethods;
import src.main.java.org.orion304.utils.RelationshipUtils;

public class Bending extends OrionPlugin {

	private static Bending plugin;

	public static final List<Material> waterbendable = new ArrayList<>();
	public static final List<Material> plantbendable = new ArrayList<>();
	public static final List<Material> earthbendable = new ArrayList<>();
	public static final List<Material> fire = new ArrayList<>();

	static {
		fire.add(Material.FIRE);

		waterbendable.add(Material.WATER);
		waterbendable.add(Material.STATIONARY_WATER);
		waterbendable.add(Material.SNOW);
		waterbendable.add(Material.ICE);
		waterbendable.add(Material.PACKED_ICE);

		plantbendable.add(Material.SAPLING);
		plantbendable.add(Material.LEAVES);
		plantbendable.add(Material.LONG_GRASS);
		plantbendable.add(Material.DEAD_BUSH);
		plantbendable.add(Material.YELLOW_FLOWER);
		plantbendable.add(Material.RED_ROSE);
		plantbendable.add(Material.BROWN_MUSHROOM);
		plantbendable.add(Material.RED_MUSHROOM);
		plantbendable.add(Material.CROPS);
		plantbendable.add(Material.CACTUS);
		plantbendable.add(Material.SUGAR_CANE_BLOCK);
		plantbendable.add(Material.MELON_BLOCK);
		plantbendable.add(Material.PUMPKIN_STEM);
		plantbendable.add(Material.MELON_STEM);
		plantbendable.add(Material.VINE);
		plantbendable.add(Material.WATER_LILY);
		plantbendable.add(Material.COCOA);
		plantbendable.add(Material.CARROT);
		plantbendable.add(Material.POTATO);
		plantbendable.add(Material.LEAVES_2);
		plantbendable.add(Material.DOUBLE_PLANT);
		plantbendable.addAll(waterbendable);

		earthbendable.add(Material.STONE);
		earthbendable.add(Material.GRASS);
		earthbendable.add(Material.DIRT);
		earthbendable.add(Material.SAND);
		earthbendable.add(Material.GRAVEL);
		earthbendable.add(Material.GOLD_ORE);
		earthbendable.add(Material.IRON_ORE);
		earthbendable.add(Material.COAL_ORE);
		earthbendable.add(Material.LAPIS_ORE);
		earthbendable.add(Material.SANDSTONE);
		earthbendable.add(Material.DIAMOND_ORE);
		earthbendable.add(Material.REDSTONE_ORE);
		earthbendable.add(Material.GLOWING_REDSTONE_ORE);
		earthbendable.add(Material.CLAY);
		earthbendable.add(Material.MYCEL);
		earthbendable.add(Material.EMERALD_ORE);
		earthbendable.add(Material.HARD_CLAY);

	}

	public static void disableAllBending() {
		for (BendingPlayer player : plugin.handler.getCustomPlayers()) {
			if (player.earthSource != null) {
				player.earthSource.kill();
			}
		}
		Progression.killAll();
		StoredBlocks.replaceAll();
		Ice.destroyAll();
	}

	public static Bending getPlugin() {
		return plugin;
	}

	public static boolean isBendable(LivingEntity entity, Location location, AbilityType type) {
		Player player = null;
		if (entity instanceof Player) {
			player = (Player) entity;
		}
		return isBendable(player, location, type);
	}

	public static boolean isBendable(Player player, Location location, AbilityType type) {
		return true;
	}

	public CustomPlayerHandler<Bending, BendingPlayer> handler;

	public SQLHandler sqlHandler;

	@Override
	public void disable() {
		disableAllBending();
	}

	@Override
	public void enable() {
		plugin = this;
		this.sqlHandler = new SQLHandler(this, "localhost", 3306, "bending", "root", "fagba11z");
		this.handler = new CustomPlayerHandler<Bending, BendingPlayer>(this, BendingPlayer.class);

		this.manager.registerEvents(new BendingListener(this), this);

		this.scheduler.runTaskTimer(this, new MainThread(this), 0L, 1L);

	}

	@Override
	public CustomPlayerHandler<Bending, BendingPlayer> getCustomPlayerHandler() {
		return this.handler;
	}

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String lbl, final String[] args) {
		String label = cmd.getName();
		if (sender instanceof Player) {
			Player player = (Player) sender;
			BendingPlayer bPlayer = handler.getCustomPlayer(player);
			try {
				if (label.equalsIgnoreCase("particle")) {
					Location location = RelationshipUtils.getTargetedLocation(player, 4);
					CraftMethods.sendPacket(this, location.getWorld(),
							CraftMethods.getParticlePacket(EnumParticle.valueOf(args[0]), true, location, 0F, 0F, 0F,
									Float.parseFloat(args[1]), Integer.parseInt(args[2])));
				} else if (label.equalsIgnoreCase("sound")) {
					player.getWorld().playSound(player.getLocation(), Sound.valueOf(args[0].toUpperCase()), 1F,
							Float.parseFloat(args[1]));
				} else if (label.equalsIgnoreCase("csound")) {
					Location location = player.getLocation();
					BendingSoundManager.playTestSound(location);

				}
			} catch (Exception e) {
				sender.sendMessage("Nope");
			}

			if (label.equalsIgnoreCase("bending")) {
				bPlayer.openBendingMenu();
			} else if (label.equalsIgnoreCase("test")) {
				Location location = RelationshipUtils.getTargetedLocation(player, 10);
				location = location.getBlock().getLocation();
				FakeBlock b = new FakeBlock(this, location, Material.DIRT);
				b.spawn();
			}
		}
		return true;
	}

}
