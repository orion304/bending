package main.java.org.valor.bending.main;

import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import main.java.org.valor.bending.abilities.AbilityType;
import main.java.org.valor.bending.abilities.earth.StoredBlocks;
import main.java.org.valor.bending.abilities.water.Ice;
import src.main.java.org.orion304.utils.EnvironmentUtils;
import src.main.java.org.orion304.utils.RelationshipUtils;

public class BendingTools {

	private static final BlockFace[] faces = { BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST };

	public static void clearBlock(Block block) {
		if (isAdjacentTo2WaterSources(block)) {
			block.setType(Material.WATER);
			block.setData((byte) 0x0);
		} else {
			block.setType(Material.AIR);
		}
	}

	public static int getEarthbendableLength(LivingEntity player, Block block, Vector direction, int maxlength,
			boolean includeMovedBlocks) {
		Location location = block.getLocation().add(.5, .5, .5);
		direction = direction.clone().normalize();
		for (int i = 1; i <= maxlength; i++) {
			Block b = location.add(direction).getBlock();
			if (!Bending.earthbendable.contains(b.getType()) || (!includeMovedBlocks && StoredBlocks.hasBeenMoved(b))
					|| !Bending.isBendable(player, b.getLocation(), AbilityType.RAISE_EARTH)) {
				return i - 1;
			}
		}
		return maxlength;
	}

	public static Block getEarthSourceBlock(LivingEntity entity, double range) {
		Set<Material> set = null;
		Block block = entity.getTargetBlock(set, (int) range);
		if (!Bending.earthbendable.contains(block.getType())) {
			block = RelationshipUtils.getSpecificTargetedBlock(entity, range, Bending.earthbendable);
		}
		return block;
	}

	public static int getPacketData(Block block) {
		if (block.getType() == Material.GRASS) {
			return 3;
		} else {
			return block.getTypeId() | (block.getData() << 0x10);
		}
	}

	public static boolean isAdjacentTo2WaterSources(Block block) {
		int sources = 0;
		Block up = block.getRelative(BlockFace.UP);
		if (EnvironmentUtils.isWater(up) && up.getData() == 0x0) {
			return true;
		}
		for (BlockFace face : faces) {
			Block b = block.getRelative(face);
			if (EnvironmentUtils.isWater(b) && b.getData() == 0x0) {
				sources++;
			}

			if (sources >= 2) {
				return true;
			}
		}
		return false;
	}

	public static boolean isEarthbendable(Block block) {
		return (Bending.earthbendable.contains(block.getType()));
	}

	public static boolean isWaterbendable(Block block, Player player) {
		return isWaterbendable(block.getType(), player);
	}

	public static boolean isWaterbendable(Material type, Player player) {
		return (Bending.waterbendable.contains(type)
				|| Bending.plantbendable.contains(type) && player.hasPermission("bending.water.plantbending"));

	}

	public static void pullWaterFromSource(Block block) {
		if (Ice.isIce(block)) {
			Ice.destroy(block);
		}
		if (!Bending.waterbendable.contains(block.getType())) {
			StoredBlocks.destroyBlock(block);
		} else {
			clearBlock(block);
		}
	}

}
