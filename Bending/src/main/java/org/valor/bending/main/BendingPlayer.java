package main.java.org.valor.bending.main;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import main.java.org.valor.bending.abilities.AbilityProjectile;
import main.java.org.valor.bending.abilities.AbilityType;
import main.java.org.valor.bending.abilities.Element;
import main.java.org.valor.bending.abilities.Progression;
import main.java.org.valor.bending.abilities.air.AirBlast;
import main.java.org.valor.bending.abilities.air.AirBurst;
import main.java.org.valor.bending.abilities.air.AirDeflect;
import main.java.org.valor.bending.abilities.air.AirPunch;
import main.java.org.valor.bending.abilities.air.AirShield;
import main.java.org.valor.bending.abilities.air.AirSpout;
import main.java.org.valor.bending.abilities.air.AirSuction;
import main.java.org.valor.bending.abilities.air.AirSwipe;
import main.java.org.valor.bending.abilities.air.Mobility;
import main.java.org.valor.bending.abilities.air.Tornado;
import main.java.org.valor.bending.abilities.earth.EarthBlast;
import main.java.org.valor.bending.abilities.earth.EarthColumn;
import main.java.org.valor.bending.abilities.earth.EarthLine;
import main.java.org.valor.bending.abilities.earth.EarthWall;
import main.java.org.valor.bending.abilities.earth.FloatingRock;
import main.java.org.valor.bending.abilities.fire.FireBlast;
import main.java.org.valor.bending.abilities.water.Ice;
import main.java.org.valor.bending.abilities.water.WaterBlast;
import main.java.org.valor.bending.abilities.water.WaterSpout;
import main.java.org.valor.bending.abilities.water.Wave;
import net.minecraft.server.v1_12_R1.EnumParticle;
import src.main.java.org.orion304.holographicmenu.HolographicMenu;
import src.main.java.org.orion304.holographicmenu.HolographicMenuChoice;
import src.main.java.org.orion304.player.CustomPlayer;
import src.main.java.org.orion304.player.CustomPlayerHandler;
import src.main.java.org.orion304.player.Hologram;
import src.main.java.org.orion304.utils.CraftMethods;
import src.main.java.org.orion304.utils.EnvironmentUtils;
import src.main.java.org.orion304.utils.MathUtils;
import src.main.java.org.orion304.utils.RelationshipUtils;

public class BendingPlayer extends CustomPlayer<Bending> {

	private static final HashSet<Byte> air = new HashSet<>();
	private static final Map<AbilityType, Long> abilityCooldowns = new HashMap<>();
	private static final PotionEffect speed = new PotionEffect(PotionEffectType.SPEED, 4, 1);

	private static final PotionEffect jump = new PotionEffect(PotionEffectType.JUMP, 4, 2);

	static {
		air.add((byte) 0);

		abilityCooldowns.put(AbilityType.AIR_BLAST, 20L);
		abilityCooldowns.put(AbilityType.AIR_SUCTION, 20L);
		abilityCooldowns.put(AbilityType.EARTH_BLAST, 20L);
		abilityCooldowns.put(AbilityType.FIRE_BLAST, 30L);
		abilityCooldowns.put(AbilityType.WATER_BLAST, 20L);
		abilityCooldowns.put(AbilityType.AIR_SHIELD, 25L);
		abilityCooldowns.put(AbilityType.MOBILITY, 20L);
		abilityCooldowns.put(AbilityType.AIR_BURST, 20L);
		abilityCooldowns.put(AbilityType.AIR_SWIPE, 30L);
		abilityCooldowns.put(AbilityType.EARTH_LINE, 40L);
		abilityCooldowns.put(AbilityType.WAVE, 20L);
	}

	private static final double redirectRange = 7 * 7;

	private final List<Progression> progressions = new ArrayList<>();

	private final AbilityType[] abilities = { null, null, null, null, null, null, null, null, null };
	private final Map<Element, Boolean> elements = new HashMap<>();
	public Block waterSource = null;
	public FloatingRock earthSource = null;
	private Location airSource = null;

	private final Map<AbilityType, Long> cooldowns = new HashMap<>();
	private long globalCooldown = 0;
	private boolean canMobility = true;
	private float previousExhaustion;
	private double prevVy = 0;

	private HolographicMenu menu = null;
	private Location menuLocation = null;
	private final Map<Hologram, BendingMenu> abilityMenus = new HashMap<>();
	private final List<Hologram> titles = new ArrayList<>();
	private final BendingMenu chooseMenu = null;
	private final BendingMenu addMenu = null;

	public BendingPlayer(final UUID playerUUID, final CustomPlayerHandler<Bending, BendingPlayer> handler) {
		super(playerUUID, handler);
	}

	@Override
	protected void asyncPreLogin(final AsyncPlayerPreLoginEvent event) {
		try {
			for (Element element : Element.values()) {
				this.elements.put(element, true);
			}

			PreparedStatement statement = this.plugin.sqlHandler.getStatement("SELECT * FROM playerData WHERE uuid=?;");
			statement.setString(1, this.playerUUID.toString());
			final ResultSet set = this.plugin.sqlHandler.get(statement);
			if (set.first()) {
				for (int i = 0; i < 9; i++) {
					final String ability = set.getString("slot" + i);
					if (ability != null) {
						this.abilities[i] = AbilityType.valueOf(ability);
					}
				}
			} else {
				statement = this.plugin.sqlHandler.getStatement("INSERT INTO playerData (uuid) values (?);");
				statement.setString(1, this.playerUUID.toString());
				this.plugin.sqlHandler.update(statement);
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}

	}

	public void clickMenu() {
		if (this.menu != null) {
			HolographicMenuChoice choice = this.menu.getChoice();
			Hologram hologram = choice.getHologram();
			int i = choice.getIndex();
			if (this.abilityMenus.containsKey(hologram)) {
				BendingMenu menu = this.abilityMenus.get(hologram);
				AbilityType ability = (AbilityType) menu.getChoice(i);
				if (ability != null) {
					int slot = this.player.getInventory().getHeldItemSlot();
					if (ability == this.abilities[slot]) {
						this.abilities[slot] = null;
						sendMessage("Cleared " + ability + ChatColor.RESET + " from slot " + (slot + 1) + ".");
					} else {
						this.abilities[slot] = ability;
						sendMessage("Bound " + ability + ChatColor.RESET + " to slot " + (slot + 1) + ".");
					}
				}
				return;
			}
		}
	}

	public void cooldown() {
		cooldown(null);
	}

	private void cooldown(AbilityType ability) {
		this.globalCooldown = this.tick + 5L;
		if (ability != null && abilityCooldowns.containsKey(ability)) {
			this.cooldowns.put(ability, this.tick + abilityCooldowns.get(ability));
		}
	}

	@Override
	public void destroy() {
		save();
	}

	public AbilityType getAbility() {
		int i = this.player.getInventory().getHeldItemSlot();
		return getAbility(i);
	}

	public AbilityType getAbility(int slot) {
		return this.abilities[slot];
	}

	private Block getTargetedWaterBlock() {
		Set<Material> set = null;
		Block block = this.player.getTargetBlock(set, 15);
		if (Bending.plantbendable.contains(block.getType())
				&& this.player.hasPermission("bending.water.plantbending")) {
			return block;
		} else if (Bending.waterbendable.contains(block.getType())) {
			return block;
		}

		if (this.player.hasPermission("bending.water.plantbending")) {
			block = RelationshipUtils.getSpecificTargetedBlock(this.player, 15, Bending.plantbendable);
		} else {
			block = RelationshipUtils.getSpecificTargetedBlock(this.player, 15, Bending.waterbendable);
		}
		return block;
	}

	@Override
	protected void handle() {
		Set<AbilityType> keys = new HashSet<>();
		keys.addAll(this.cooldowns.keySet());
		for (AbilityType type : keys) {
			if (this.tick > this.cooldowns.get(type)) {
				this.cooldowns.remove(type);
			}
		}

		if (this.tick > this.globalCooldown) {
			this.globalCooldown = 0;
		}

		if (this.airSource != null) {
			if (!this.airSource.getWorld().equals(this.player.getWorld())
					|| this.airSource.distanceSquared(this.player.getLocation()) > 400) {
				this.airSource = null;
			} else {
				sendPacket(CraftMethods.getParticlePacket(EnumParticle.SMOKE_NORMAL, true, this.airSource, .1F, .1F,
						.1F, 0, 2));
			}
		}

		if (this.menuLocation != null) {
			if (!this.player.getWorld().equals(this.menuLocation.getWorld())
					|| this.player.getLocation().distanceSquared(this.menuLocation) > 1) {
				openBendingMenu();
			}
		}

		List<Progression> progressionKeys = new ArrayList<>();
		progressionKeys.addAll(this.progressions);
		for (Progression progression : progressionKeys) {
			if (!progression.isAlive) {
				this.progressions.remove(progression);
			}
		}

		if (isReady(AbilityType.MOBILITY) && !this.canMobility && ((LivingEntity) this.player).isOnGround()) {
			this.canMobility = true;
		}

		if (isBender(Element.AIR)) {
			if (this.player.hasPermission("bending.air.hunger")) {
				float exhaustion = this.player.getExhaustion();
				float change = exhaustion - this.previousExhaustion;
				if (change > 0) {
					this.player.setExhaustion(this.previousExhaustion + change * .2F);
				}
			}

			if (this.player.isSprinting() && this.player.hasPermission("bending.air.speed")) {
				this.player.addPotionEffect(speed);
				this.player.addPotionEffect(jump);
			}

		}

		this.previousExhaustion = this.player.getExhaustion();
		this.prevVy = getVelocity().getY();

		// ServerUtils.verbose(this.player.getName() + ": "
		// + getVelocity().lengthSquared());
	}

	@Override
	public void initialize() {
		this.player.setFlySpeed(.2F);
		this.previousExhaustion = this.player.getExhaustion();
	}

	public boolean isBender(Element element) {
		return this.elements.get(element);
	}

	private boolean isReady(AbilityType type) {
		return this.globalCooldown == 0 && !this.cooldowns.containsKey(type);
	}

	public boolean isSpouting() {
		for (WaterSpout spout : Progression.getByClass(WaterSpout.class)) {
			if (spout.player.equals(this.player)) {
				return true;
			}
		}

		for (AirSpout spout : Progression.getByClass(AirSpout.class)) {
			if (spout.player.equals(this.player)) {
				return true;
			}
		}

		return false;
	}

	@Override
	protected void login(final PlayerLoginEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	protected int maxVotes() {
		return 0;
	}

	public void openBendingMenu() {
		if (this.menu != null) {
			this.menu.destroy();
			sendMessage(ChatColor.GOLD + "Bending Menu closed.");
			for (Hologram hologram : this.titles) {
				hologram.destroy();
			}
			this.titles.clear();
			this.menu = null;
			this.menuLocation = null;
		} else {
			this.menu = new HolographicMenu(this.plugin, this);
			this.menuLocation = this.player.getLocation().clone();

			List<Element> elements = new ArrayList<>();
			for (Element element : this.elements.keySet()) {
				if (this.player.hasPermission(element.getPermissionNode())) {
					elements.add(element);
				}
			}

			int size = elements.size();
			if (this.player.hasPermission("bending.command.choose")) {
				size++;
			}
			if (this.player.hasPermission("bending.command.add")) {
				size++;
			}

			if (size == 0) {
				return;
			}

			double angle = 0;
			double inc = -30;
			Location origin = this.player.getEyeLocation();
			origin.add(0, .75, 0);
			Vector direction = origin.getDirection();
			direction.setY(0);
			direction.normalize().multiply(5);
			Vector up = new Vector(0, 1, 0);
			List<String> abilities = new ArrayList<>();
			this.abilityMenus.clear();
			for (Element element : elements) {
				Location location = origin.clone().add(MathUtils.rotateVectorAroundVector(up, direction, angle));
				Hologram title = new Hologram(this.plugin, location, element + " Abilities");
				this.showHologram(title);
				this.titles.add(title);
				location.subtract(0, Hologram.distance, 0);
				int i = 0;
				BendingMenu bendingMenu = new BendingMenu();
				for (AbilityType ability : AbilityType.getAbilities(element)) {
					if (this.player.hasPermission(ability.getPermissionNode())) {
						abilities.add(ability.toString());
						bendingMenu.setChoice(i, ability);
						i++;
					}
				}
				this.abilityMenus.put(this.menu.addPane(location, abilities), bendingMenu);
				angle += inc;
				abilities.clear();
			}

			angle = 0;
			if (this.player.hasPermission("bending.command.add")) {
				angle -= inc;

			}
			this.menu.show();
			sendMessage(ChatColor.GOLD
					+ "This is the Bending menu. Click on the ability to bind them to the slot you've selected. Clicking the ability you have bound will clear that slot. Use /bending or walk away to remove the menu. You cannot bend while the menu is running.");
			for (int i = 0; i < 9; i++) {
				AbilityType ability = this.abilities[i];
				if (ability != null) {
					sendMessage("Slot " + (i + 1) + ": " + ability);
				}
			}
		}

	}

	@Override
	protected void save() {
		final StringBuilder statement = new StringBuilder();
		statement.append("UPDATE playerData SET ");

		for (int i = 0; i < 9; i++) {
			statement.append("slot");
			statement.append(i);
			statement.append("=");
			final AbilityType type = this.abilities[i];
			if (type == null) {
				statement.append("null");
			} else {
				statement.append("'");
				statement.append(type.name());
				statement.append("'");
			}
			if (i != 8) {
				statement.append(", ");
			}
		}
		statement.append(" WHERE uuid='");
		statement.append(this.playerUUID);
		statement.append("';");
		try {
			this.plugin.sqlHandler.update(statement.toString());
		} catch (final SQLException e) {
			e.printStackTrace();
		}

		// create table playerData (uuid VARCHAR(40), slot0 VARCHAR(30), slot1
		// VARCHAR(30), slot2 VARCHAR(30), slot3 VARCHAR(30), slot4 VARCHAR(30),
		// slot5 VARCHAR(30), slot6 VARCHAR(30), slot7 VARCHAR(30), slot8
		// VARCHAR(30))
	}

	private void selectEarthSource() {
		Block block = BendingTools.getEarthSourceBlock(this.player, 8);

		if (block != null && Bending.isBendable(this.player, block.getLocation(), getAbility())) {
			this.waterSource = null;
			if (this.earthSource != null) {
				this.earthSource.drop();
			}
			this.earthSource = new FloatingRock(block);
		}
	}

	private void selectWaterSource() {
		Block block = getTargetedWaterBlock();
		if (block != null && Bending.isBendable(this.player, block.getLocation(), getAbility())
				&& Ice.isSource(block, this.player)) {

			if (this.earthSource != null) {
				this.earthSource.drop();
				this.earthSource = null;
			}

			this.waterSource = block;
		}
	}

	public void sneak(boolean nowSneaking) {
		if (!nowSneaking) {
			return;
		}
		if (this.menu != null) {
			return;
		}
		AbilityType type = getAbility();
		if (type == null) {
			return;
		}
		if (!this.player.hasPermission(type.getPermissionNode())) {
			return;
		}
		switch (type) {
		case AIR_BLAST:
		case AIR_SUCTION: {
			this.airSource = RelationshipUtils.getTargetedLocation(this.player, 15, EnvironmentUtils.nonSolidNonLiquid);
			if (this.airSource != null && !Bending.isBendable(this.player, this.airSource, type)) {
				this.airSource = null;
			}
			break;
		}
		case AIR_SHIELD: {
			this.progressions.add(new AirShield(this));
			break;
		}
		case AIR_BURST: {
			this.progressions.add(new AirBurst(this));
			break;
		}
		case TORNADO: {
			this.progressions.add(new Tornado(this));
			break;
		}
		case MOBILITY: {
			new Mobility(this, true);
			break;
		}
		case WAVE: {
			{
				selectWaterSource();
				break;
			}
		}
		case WATER_BLAST:
			WaterBlast blast = null;
			for (AbilityProjectile projectile : AbilityProjectile.projectiles) {
				if (projectile instanceof WaterBlast && !projectile.getShooter().equals(this.player)
						&& projectile.getLocation().distanceSquared(this.player.getLocation()) <= redirectRange) {
					blast = (WaterBlast) projectile;
					blast.redirect(this.player);
				}
			}
			if (blast == null) {
				if (this.waterSource != null && getTargetedWaterBlock() == null) {
					new WaterBlast(this, true);
				} else {
					selectWaterSource();
				}
			}
			break;
		case EARTH_BLAST:
			selectEarthSource();
			break;
		case RAISE_EARTH: {
			if (isReady(type)) {
				new EarthWall(this.player, RelationshipUtils
						.getTargetedLocation(this.player, 15, EnvironmentUtils.transparent).getBlock());
				cooldown(type);
			}
			break;
		}
		default:
			break;
		}
	}

	@Override
	protected void turnOffSpectating() {
		// TODO Auto-generated method stub
	}

	@Override
	protected void turnOnSpectating() {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean useItem(final int slot, final ItemStack item, final Action action) {
		if (this.menu != null) {
			return false;
		}
		if (action != Action.LEFT_CLICK_AIR && action != Action.LEFT_CLICK_BLOCK) {
			return false;
		}
		AbilityType type = getAbility(slot);
		if (type == null) {
			return false;
		}
		if (!this.player.hasPermission(type.getPermissionNode())) {
			return false;
		}
		switch (type) {
		case AIR_BLAST: {
			if (isReady(type)) {
				if (this.airSource != null) {
					Entity target = RelationshipUtils.getTargetedEntity(this.player, 50, LivingEntity.class);
					Location destination;
					if (target == null) {
						destination = RelationshipUtils.getTargetedLocation(this.player, 50);
					} else {
						destination = target.getLocation();
					}
					new AirBlast(this.player, this.airSource, MathUtils.getDistanceVector(this.airSource, destination));
					this.airSource = null;
				} else {
					new AirBlast(this.player);
				}
				cooldown(type);
			}
			break;
		}
		case AIR_SUCTION: {
			if (isReady(type)) {
				Entity target = RelationshipUtils.getTargetedEntity(this.player, 20, LivingEntity.class);
				Location destination = RelationshipUtils.getTargetedLocation(this.player, 20,
						EnvironmentUtils.nonSolidNonLiquid);
				if (target != null && target.getLocation().distanceSquared(this.player.getLocation()) < destination
						.distanceSquared(this.player.getLocation())) {
					destination = target.getLocation();
				}
				if (this.airSource != null) {
					new AirSuction(this.player, destination, MathUtils.getDistanceVector(destination, this.airSource),
							isSpouting());
					this.airSource = null;
				} else {
					new AirSuction(this.player, destination,
							MathUtils.getDistanceVector(destination, this.player.getEyeLocation()), isSpouting());
				}
				cooldown(type);
			}
			break;
		}
		case AIR_SHIELD: {
			if (isReady(type)) {
				this.progressions.add(new AirDeflect(this.player));
				cooldown(type);
			}
			break;
		}
		case TORNADO: {
			for (Progression progression : this.progressions) {
				if (progression instanceof Tornado) {
					Tornado tornado = (Tornado) progression;
					if (!tornado.isDirected()) {
						tornado.direct();
					}
				}
			}
			break;
		}
		case AIR_SPOUT: {
			if (isReady(type)) {
				new AirSpout(this.player);
				cooldown(type);
			}
			break;
		}
		case MOBILITY: {
			if (this.canMobility && isReady(type)) {
				new Mobility(this, false);
				cooldown(type);
				this.canMobility = false;
			}
			break;
		}
		case AIR_BURST: {
			if (isReady(type)) {
				new AirPunch(this);
				cooldown(type);
			}
			break;
		}
		case AIR_SWIPE: {
			if (isReady(type)) {
				new AirSwipe(this.player);
				cooldown(type);
			}
			break;
		}
		case EARTH_BLAST: {
			for (AbilityProjectile projectile : AbilityProjectile.projectiles) {
				if (projectile instanceof EarthBlast && !projectile.getShooter().equals(this.player)
						&& projectile.getLocation().distanceSquared(this.player.getLocation()) <= redirectRange) {
					projectile.kill();
				}
			}
			if (this.earthSource != null && isReady(type)) {
				new EarthBlast(this, this.earthSource);
				cooldown(type);
			}
			break;
		}
		case RAISE_EARTH: {
			if (isReady(type) && BendingTools.getEarthSourceBlock(this.player, 20) != null) {
				this.progressions.add(new EarthColumn(this.player));
				cooldown(type);
			}
			break;
		}
		case EARTH_LINE: {
			if (isReady(type)) {
				this.progressions.add(new EarthLine(this.player));
				cooldown(type);
			}
			break;
		}
		case FIRE_BLAST: {
			if (isReady(type)) {
				new FireBlast(this.player);
				cooldown(type);
			}
			break;
		}
		case WATER_BLAST: {
			for (AbilityProjectile projectile : AbilityProjectile.projectiles) {
				if (projectile instanceof WaterBlast && !projectile.getShooter().equals(this.player)
						&& projectile.getLocation().distanceSquared(this.player.getLocation()) <= redirectRange) {
					projectile.kill();
				}
			}
			if (this.waterSource != null && isReady(type)) {
				new WaterBlast(this, false);
				cooldown(type);
			}
			break;
		}
		case WATER_SPOUT: {
			if (isReady(type)) {
				new WaterSpout(this.player);
				cooldown(type);
			}
			break;
		}
		case WAVE: {
			if (this.waterSource != null && isReady(type)) {
				new Wave(this);
				cooldown(type);
				break;
			}

			for (Wave wave : Progression.getByClass(Wave.class)) {
				if (wave.player.equals(this)) {
					wave.freeze();
				}
			}
		}
		default:
			break;
		}
		return false;
	}

}
