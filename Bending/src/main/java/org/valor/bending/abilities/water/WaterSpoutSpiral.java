package main.java.org.valor.bending.abilities.water;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.LivingEntity;

import net.minecraft.server.v1_12_R1.Entity;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_12_R1.PacketPlayOutSpawnEntity;
import src.main.java.org.orion304.utils.CraftMethods;

public class WaterSpoutSpiral {

	public static final double dtheta = Math.toRadians(30);

	private Entity entity;
	private double y;
	public double angle;
	public final double yAnchor;
	private World world;
	private LivingEntity source;
	public double previousDy;

	public WaterSpoutSpiral(LivingEntity source, double y, double angle, double yAnchor) {
		this.yAnchor = yAnchor;
		this.y = y;
		this.angle = angle;
		this.world = source.getWorld();
		this.source = source;
		this.entity = CraftMethods.getNewEntity(getLocation());

	}

	public List<Packet<?>> getDestroyPackets() {
		List<Packet<?>> packets = new ArrayList<>();
		packets.add(new PacketPlayOutEntityDestroy(this.entity.getId()));
		return packets;
	}

	public Entity getEntity() {
		return this.entity;
	}

	public Location getLocation() {
		Location loc = this.source.getLocation();
		double x = Math.cos(this.angle) + loc.getX();
		double z = Math.sin(this.angle) + loc.getZ();
		return new Location(this.world, x, this.y, z);
	}

	public List<Packet<?>> getSpawnPackets() {
		List<Packet<?>> packets = new ArrayList<>();
		// packets.add(new PacketPlayOutSpawnEntity(this.entity, 70, 8));
		packets.add(new PacketPlayOutSpawnEntity(this.entity, 70, 79));
		return packets;
	}

	public void progress(double dy) {
		this.angle += dtheta;
		this.y += dy;
		this.previousDy = dy;
		Location loc = getLocation();
		this.entity.setLocation(loc.getX(), loc.getY(), loc.getZ(), 0, 0);
	}

}
