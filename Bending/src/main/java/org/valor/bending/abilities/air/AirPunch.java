package main.java.org.valor.bending.abilities.air;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import main.java.org.valor.bending.abilities.AbilityProjectile;
import main.java.org.valor.bending.abilities.AbilityType;
import main.java.org.valor.bending.abilities.Progression;
import main.java.org.valor.bending.main.BendingPlayer;
import net.minecraft.server.v1_12_R1.EnumParticle;
import src.main.java.org.orion304.utils.CraftMethods;
import src.main.java.org.orion304.utils.EnvironmentUtils;
import src.main.java.org.orion304.utils.MathUtils;
import src.main.java.org.orion304.utils.RelationshipUtils;

public class AirPunch extends AbilityProjectile {

	// y = 4*h*x/d (1 - x/d)
	// y' = 4*h/d - 8*h*x/d^2
	private static final Vector up = new Vector(0, 1, 0);
	private static final double h = 1;
	private static final double speed = 3;

	private final Vector xhat, yhat;
	private final double d, invd, a, b;

	public AirPunch(BendingPlayer player) {
		super(AbilityType.AIR_BURST, player.getPlayer().getEyeLocation(),
				player.getPlayer().getEyeLocation().getDirection(), player
						.getPlayer(), false, true, true, .2);
		Player entity = player.getPlayer();
		LivingEntity target = RelationshipUtils.getTargetedEntity(entity, 25,
				LivingEntity.class);
		Location destination;
		if (target != null) {
			destination = target.getLocation().add(0, 1, 0);
		} else {
			destination = RelationshipUtils.getTargetedLocation(entity, 25,
					EnvironmentUtils.nonSolidNonLiquid);
		}

		this.xhat = MathUtils.getDistanceVector(this.location, destination)
				.normalize();
		this.yhat = MathUtils.rotateVectorAroundVector(up, this.xhat, 90);
		if (player.getVelocity().dot(this.yhat) > 0) {
			this.yhat.multiply(-1);
		}
		this.d = this.location.distance(destination);
		this.invd = 1 / this.d;
		this.a = 4 * h * this.invd;
		this.b = 8 * h * this.invd * this.invd;
		updateVelocity();
	}

	@Override
	protected void animate() {
		for (AirShield shield : Progression.getByClass(AirShield.class)) {
			if (shield.isInShield(this.location)) {
				kill();
				return;
			}
		}
		updateVelocity();
		CraftMethods.sendPacket(this.plugin, this.location.getWorld(),
				CraftMethods.getParticlePacket(EnumParticle.SMOKE_NORMAL, true,
						this.location, 0, 0, 0, 0, 1));
	}

	private void updateVelocity() {
		Vector distance = MathUtils.getDistanceVector(this.source,
				this.location);
		double x = distance.distance(this.xhat);
		double yprime = this.a - this.b * x;
		double vx = speed / Math.sqrt(1 + yprime * yprime);
		double vy = yprime * vx;
		this.velocity = this.xhat.clone().multiply(vx)
				.add(this.yhat.clone().multiply(vy));
	}

}
