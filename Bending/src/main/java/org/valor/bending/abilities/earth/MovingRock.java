package main.java.org.valor.bending.abilities.earth;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.util.Vector;

import main.java.org.valor.bending.main.Bending;
import main.java.org.valor.bending.main.BendingPlayer;
import src.main.java.org.orion304.Hitbox;
import src.main.java.org.orion304.fakeentity.FakeBlock;
import src.main.java.org.orion304.utils.MathUtils;

public class MovingRock {

	private static final List<MovingRock> instances = new ArrayList<>();

	public static boolean isBlocked(Hitbox hitbox) {
		for (MovingRock rock : instances) {
			if (hitbox.isInside(rock.hitbox)) {
				return true;
			}
		}

		for (BendingPlayer player : Bending.getPlugin().handler.getCustomPlayers()) {

			if (player.earthSource != null && player.earthSource.hitbox.isInside(hitbox)) {
				return true;
			}
		}
		return false;
	}

	public static void progressAll() {
		List<MovingRock> toRemove = new ArrayList<>();

		for (MovingRock rock : instances) {
			if (rock.isAlive()) {
				rock.handle();
			}
			if (!rock.isAlive()) {
				toRemove.add(rock);
			}
		}

		instances.removeAll(toRemove);
	}

	private Block start, end;
	public BlockState state;
	private Vector velocity;
	private Location source, location, destination;

	// private Entity block;
	private FakeBlock fakeBlock;
	// private int packetData;

	private double maxDistance;

	private Material material;
	private byte data;

	private boolean isRunning = true;
	boolean revert;
	private final Hitbox hitbox = new Hitbox(.5, .5);
	private boolean returnToSource;

	public MovingRock(Block start, Block end, double speed) {
		this(start, end, speed, true);
	}

	public MovingRock(Block start, Block end, double speed, boolean revert) {
		this(start, end, speed, revert, false);
	}

	public MovingRock(Block start, Block end, double speed, boolean revert, boolean returnToSource) {
		this(start.getState(), start.getLocation().add(.5, 0, .5), end, speed, revert);
		if (!start.equals(end)) {
			start.setType(Material.AIR);
		}
	}

	public MovingRock(BlockState startState, Location startLocation, Block end, double speed, boolean revert) {
		this(startState, startLocation, end, speed, revert, false);
	}

	public MovingRock(BlockState startState, Location startLocation, Block end, double speed, boolean revert,
			boolean returnToSource) {
		this.start = startState.getBlock();
		this.end = end;
		this.revert = revert;
		this.returnToSource = returnToSource;

		this.source = startLocation.clone();
		this.location = this.source.clone();
		this.location.setPitch(0);
		this.location.setYaw(0);
		this.hitbox.setCenterLocation(this.location);
		this.destination = end.getLocation().clone().add(.5, 0, .5);
		this.velocity = MathUtils.getDistanceVector(this.source, this.destination).normalize().multiply(speed);

		this.maxDistance = this.location.distanceSquared(this.destination);

		this.material = startState.getType();
		this.data = startState.getRawData();
		// if (this.material == Material.GRASS) {
		// this.packetData = 3;
		// } else {
		// this.packetData = startState.getTypeId() | (this.data << 0x10);
		// }

		this.state = startState;

		if (startLocation.distanceSquared(this.destination) > this.velocity.lengthSquared()) {
			fakeBlock = new FakeBlock(Bending.getPlugin(), this.location, material, data);
			fakeBlock.setVelocity(velocity);
			// this.block = CraftMethods.getNewEntity(this.location);
			spawn();

			instances.add(this);
		} else {
			startState.update(true);
		}
	}

	public void destroy() {
		kill();
		if (this.revert) {
			this.state.update(true);
			StoredBlocks.destroyBlock(this.start);
		}
	}

	public Location getLocation() {
		return this.location;
	}

	public Vector getVelocity() {
		return this.velocity;
	}

	public void handle() {
		if (this.isRunning) {
			this.location.add(this.velocity);
			// this.block.setLocation(this.location.getX(),
			// this.location.getY(), this.location.getZ(), 0, 0);

			fakeBlock.setLocation(this.location);
			if (this.location.distanceSquared(this.source) >= this.maxDistance) {
				if (this.returnToSource) {
					setNewDestination(this.source.clone());
					this.returnToSource = false;
				} else {
					kill();
					if (this.revert) {
						this.end.setType(this.material);
						this.end.setData(this.data);
						StoredBlocks.moveBlock(this.state, this.end);
					}
				}
			} else if (this.location.getBlock().getType().isSolid()) {
				if (this.returnToSource) {
					setNewDestination(this.source.clone());
					this.returnToSource = false;
				} else {
					kill();
					if (this.revert) {
						BlockState currentState = this.state.getBlock().getState();
						this.state.update(true);
						StoredBlocks.destroyBlock(this.start);
						currentState.update(true);
					}
				}
			} else {
				// spawn();
				this.hitbox.setCenterLocation(this.location);
			}
		}
	}

	public boolean isAlive() {
		return this.isRunning;
	}

	private void kill() {
		this.isRunning = false;
		fakeBlock.despawn();
		fakeBlock.destroy();
		// CraftMethods.sendPacket(Bending.getPlugin(),
		// this.location.getWorld(),
		// new PacketPlayOutEntityDestroy(this.block.getId()));
	}

	public void setNewDestination(Location location) {
		this.destination = location;
		this.source = this.location.clone();
		this.end = location.getBlock();
		this.maxDistance = location.distanceSquared(this.location);
		this.velocity = MathUtils.getDistanceVector(this.source, this.destination).normalize()
				.multiply(this.velocity.length());
	}

	private void spawn() {
		fakeBlock.spawn();
		// this.block.setLocation(this.location.getX(), this.location.getY(),
		// this.location.getZ(), 0, 0);
		this.hitbox.setCenterLocation(this.location);
		// CraftMethods.sendPacket(Bending.getPlugin(),
		// this.location.getWorld(),
		// new PacketPlayOutSpawnEntity(this.block, 70, this.packetData));
	}

}
