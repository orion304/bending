package main.java.org.valor.bending.abilities.earth;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

import main.java.org.valor.bending.abilities.AbilityProjectile;
import main.java.org.valor.bending.abilities.Progression;
import main.java.org.valor.bending.abilities.air.AirSpout;
import main.java.org.valor.bending.abilities.water.WaterSpout;
import main.java.org.valor.bending.main.BendingTools;
import main.java.org.valor.bending.sounds.BendingSoundManager;
import main.java.org.valor.bending.sounds.BendingSounds;
import src.main.java.org.orion304.Hitbox;
import src.main.java.org.orion304.utils.EnvironmentUtils;
import src.main.java.org.orion304.utils.MathUtils;

public class EarthLine extends Progression {

	private static final Vector up = new Vector(0, 1, 0);

	private static final double distance = 25;
	private static final double distanceSquared = distance * distance;
	private static final double speed = .8D;
	private static final double damage = 3D;

	public static boolean isBlocked(Hitbox hitbox) {
		for (EarthLine line : Progression.getByClass(EarthLine.class)) {
			if (line.isInLine(hitbox)) {
				return true;
			}
		}
		return false;
	}

	private final Location origin;
	private Location location;
	private final Vector velocity;
	private final Hitbox hitbox = new Hitbox(500, distance);
	private final LivingEntity source;
	private final List<Protrusion> protrusions = new ArrayList<>();
	private final List<Entity> affectedEntities = new ArrayList<>();

	public EarthLine(LivingEntity entity) {
		super();
		this.location = entity.getLocation();
		this.location = this.location.getBlock().getRelative(BlockFace.DOWN).getLocation().add(.5, .5, .5);
		this.origin = this.location.clone();
		this.hitbox.setCenterLocation(this.origin);
		this.velocity = entity.getEyeLocation().getDirection();
		this.velocity.setY(0);
		this.velocity.normalize().multiply(speed);
		advance();
		this.source = entity;
		this.isAlive = true;
	}

	private boolean advance() {
		Block firstBlock = this.location.getBlock();
		this.location.add(this.velocity);
		if (this.location.getBlock().equals(firstBlock)) {
			return false;
		} else {
			findSource();
			return true;
		}
	}

	private void affect(Entity entity, Vector velocity) {
		Vector v = this.velocity.clone();
		Vector offset = velocity.clone();
		offset.setY(0);
		if (offset.lengthSquared() != 0) {
			offset.normalize().multiply(.7);
		}
		v.add(offset);
		v.setY(.5);

		Vector vel = entity.getVelocity();
		MathUtils.setComponent(vel, v);

		if (entity instanceof LivingEntity) {
			((LivingEntity) entity).damage(damage, this.source);
		}
		entity.setVelocity(vel);
		this.affectedEntities.add(entity);
	}

	private void findSource() {
		Location loc = this.location.clone().add(0, 3, 0);
		for (int i = 3; i >= -3; i++) {
			Block block = loc.getBlock();
			if (BendingTools.isEarthbendable(block)) {
				this.location = loc;
				return;
			}
			loc.subtract(up);
		}
		this.location = null;
	}

	public boolean isInLine(Hitbox hitbox) {
		if (!hitbox.isInside(this.hitbox)) {
			return false;
		}
		for (Protrusion protrusion : this.protrusions) {
			if (protrusion.isInHitbox(hitbox)) {
				return true;
			}
		}
		return false;

	}

	@Override
	protected void progress() {
		boolean protrude = false;
		if (this.location != null) {
			protrude = advance();
		}

		if (this.location != null) {
			if (this.location.distanceSquared(this.origin) > distanceSquared) {
				this.location = null;
				return;
			}

			if (protrude) {
				Vector v = MathUtils.rotateVectorAroundVector(this.velocity, up,
						MathUtils.random.nextDouble() * 40 - 20);
				v.multiply(.5);
				this.protrusions.add(new Protrusion(this.location.getBlock(), v, 3));
			}
		}

		Set<Entity> entities = EnvironmentUtils.getEntitiesAroundPoints(this.origin, 50);
		entities.remove(this.source);
		List<Protrusion> toRemove = new ArrayList<>();
		for (Protrusion protrusion : this.protrusions) {
			if (protrusion.isAlive) {
				protrusion.progress();
			}

			if (!protrusion.isAlive) {
				toRemove.add(protrusion);
			} else {
				for (Entity entity : entities) {
					Hitbox hitbox;
					if (entity instanceof LivingEntity) {
						hitbox = new Hitbox((LivingEntity) entity);
					} else {
						hitbox = new Hitbox(.5, .5);
						hitbox.setCenterLocation(entity.getLocation());
					}
					if (!this.affectedEntities.contains(entity) && protrusion.isInHitbox(hitbox)) {
						affect(entity, protrusion.getVelocity());
					}
				}

				if (this.tick % 2 == 0) {
					Location loc = protrusion.getLocation();
					if (loc != null) {
						World world = loc.getWorld();
						BendingSoundManager.playSound(loc, BendingSounds.EARTH_COLUMN_MOVE);
					}
				}
			}
		}
		this.protrusions.removeAll(toRemove);

		if (this.protrusions.isEmpty()) {
			this.isAlive = false;
		}

		for (WaterSpout waterSpout : Progression.getByClass(WaterSpout.class)) {
			if (isInLine(waterSpout.hitbox) && !waterSpout.player.equals(this.source)) {
				waterSpout.kill();
			}
		}

		for (AirSpout airSpout : Progression.getByClass(AirSpout.class)) {
			if (isInLine(airSpout.hitbox) && !airSpout.player.equals(this.source)) {
				airSpout.kill();
			}
		}

		for (AbilityProjectile projectile : AbilityProjectile.getProjectilesInHitbox(this.hitbox)) {
			if (projectile.getShooter().equals(this.source)) {
				continue;
			}

			for (Protrusion protrusion : this.protrusions) {
				if (protrusion.isInHitbox(projectile)) {
					projectile.kill();
					break;
				}
			}
		}
	}

}
