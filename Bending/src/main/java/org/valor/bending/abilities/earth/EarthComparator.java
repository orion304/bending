package main.java.org.valor.bending.abilities.earth;

import java.util.Comparator;

import org.bukkit.block.Block;
import org.bukkit.util.Vector;

public class EarthComparator implements Comparator<Block> {

	private final int x, y, z, vx, vy, vz;

	public EarthComparator(Block origin, Vector direction) {
		this.x = origin.getX();
		this.y = origin.getX();
		this.z = origin.getX();
		this.vx = (int) Math.signum(direction.getX());
		this.vy = (int) Math.signum(direction.getY());
		this.vz = (int) Math.signum(direction.getZ());
	}

	@Override
	public int compare(Block b1, Block b2) {
		int b1x = b1.getX() - this.x;
		int b1y = b1.getY() - this.y;
		int b1z = b1.getZ() - this.z;

		int b2x = b2.getX() - this.x;
		int b2y = b2.getY() - this.y;
		int b2z = b2.getZ() - this.z;

		int sb1, sb2;

		if (this.vx == Math.signum(b1x) || this.vy == Math.signum(b1y)
				|| this.vz == Math.signum(b1z)) {
			sb1 = 1;
		} else {
			sb1 = -1;
		}

		if (this.vx == Math.signum(b2x) || this.vy == Math.signum(b2y)
				|| this.vz == Math.signum(b2z)) {
			sb2 = 1;
		} else {
			sb2 = -1;
		}

		if (sb1 < sb2) {
			return -1;
		} else if (sb1 > sb2) {
			return 1;
		} else {
			int d1 = b1x * b1x + b1y * b1y + b1z * b1z;
			int d2 = b2x * b2x + b2y * b2y + b2z * b2z;
			if (d1 == d2) {
				return 0;
			} else if (d1 < d2) {
				return sb1 == 1 ? -1 : 1;
			} else {
				return sb1 == 1 ? 1 : -1;
			}
		}
	}

}
