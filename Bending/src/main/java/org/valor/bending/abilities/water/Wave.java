package main.java.org.valor.bending.abilities.water;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

import main.java.org.valor.bending.abilities.AbilityProjectile;
import main.java.org.valor.bending.abilities.Progression;
import main.java.org.valor.bending.abilities.earth.EarthLine;
import main.java.org.valor.bending.abilities.earth.MovingRock;
import main.java.org.valor.bending.main.Bending;
import main.java.org.valor.bending.main.BendingPlayer;
import main.java.org.valor.bending.main.BendingTools;
import main.java.org.valor.bending.main.MainThread;
import main.java.org.valor.bending.sounds.BendingSoundManager;
import main.java.org.valor.bending.sounds.BendingSounds;
import net.minecraft.server.v1_12_R1.Entity;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_12_R1.PacketPlayOutSpawnEntity;
import src.main.java.org.orion304.Hitbox;
import src.main.java.org.orion304.utils.CraftMethods;
import src.main.java.org.orion304.utils.EnvironmentUtils;
import src.main.java.org.orion304.utils.MathUtils;
import src.main.java.org.orion304.utils.RelationshipUtils;

public class Wave extends Progression {

	private static final double maxRange = 25, maxRange2 = maxRange * maxRange;
	private static final double maxRadius = 2;
	private static final double speed = .7;
	private static final double growDistance = 7, growDistance2 = growDistance * growDistance;

	public BendingPlayer player;
	private Location location, source;
	private Vector velocity, push;
	private boolean growing = true;
	public final Hitbox hitbox = new Hitbox(maxRadius + 1, maxRadius + 1);
	private final Map<Entity, Vector> blocks = new HashMap<>();

	public Wave(BendingPlayer player) {
		super();
		if (player.waterSource == null) {
			this.isAlive = false;
			return;
		}
		this.isAlive = true;
		this.player = player;
		this.location = player.waterSource.getLocation().add(.5, .5, .5);
		this.source = this.location.clone();
		BendingTools.pullWaterFromSource(player.waterSource);
		player.waterSource = null;
		this.hitbox.setCenterLocation(this.location);
		createWater();
		sendPackets();
	}

	private void createWater() {
		LivingEntity target = RelationshipUtils.getTargetedEntity(this.player.getPlayer(), maxRange, LivingEntity.class,
				null);
		Location destination;
		if (target == null) {
			destination = RelationshipUtils.getTargetedLocation(this.player.getPlayer(), maxRange);
		} else {
			destination = target.getLocation().add(0, 1, 0);
		}
		Vector main = MathUtils.getDistanceVector(this.source, destination).normalize();
		Entity block = CraftMethods.getNewEntity(this.location);
		this.velocity = main.clone().multiply(speed);
		this.push = this.velocity.clone();
		if (this.push.getY() > .3) {
			this.push.setY(.3);
		}
		this.blocks.put(block, this.velocity);
		Vector ortho = MathUtils.getOrthogonalVector(main, 0, 1);
		for (double i = .5; i <= maxRadius; i += .5) {
			double dTheta = 360D / Math.ceil(2 * Math.PI * i);
			double d = Math.toDegrees(Math.atan(i / growDistance));
			for (double theta = 0; theta < 360; theta += dTheta) {
				Vector velocity = main.clone();
				ortho = MathUtils.getOrthogonalVector(velocity, theta, 1);
				velocity = MathUtils.rotateVectorAroundVector(ortho, velocity, d);
				velocity.multiply(speed / Math.cos(Math.toRadians(d)));
				block = CraftMethods.getNewEntity(this.location);
				this.blocks.put(block, velocity);
			}
		}
	}

	private void destroy() {
		List<Packet<?>> packets = new ArrayList<>();
		for (Entity block : this.blocks.keySet()) {
			packets.add(new PacketPlayOutEntityDestroy(block.getId()));
		}

		this.blocks.clear();
		this.isAlive = false;
		CraftMethods.sendPacket(Bending.getPlugin(), this.location.getWorld(), packets);
	}

	public void freeze() {
		boolean freeze = false;
		Set<Block> ice = new HashSet<>();

		Set<Block> allBlocks = EnvironmentUtils.getBlocksOnPlane(this.location, this.velocity, 2 * maxRadius + 3);
		List<Block> anchors = new ArrayList<>();
		for (Block block : allBlocks) {
			if (EnvironmentUtils.isSolid(block) || EnvironmentUtils.isWater(block)) {
				anchors.add(block);
			}
		}

		for (Entity entity : this.blocks.keySet()) {
			Block block = entity.getBukkitEntity().getLocation().getBlock();
			if (!ice.contains(block)) {
				ice.add(block);
				for (Block anchor : anchors) {
					if (!freeze && RelationshipUtils.isBlockTouching(block, anchor)) {
						freeze = true;
						break;
					}
				}
			}
		}

		if (freeze) {
			for (Block block : ice) {
				new Ice(block, this.player.getPlayer());
			}
		}

		destroy();
	}

	@Override
	protected void progress() {
		List<Hitbox> hitboxes = new ArrayList<>();
		List<Entity> toRemove = new ArrayList<>();
		for (Entity entity : this.blocks.keySet()) {
			Vector velocity = this.blocks.get(entity);
			Location loc = entity.getBukkitEntity().getLocation();
			loc.add(velocity);
			Hitbox hitbox = new Hitbox(.5, .5);
			hitbox.setCenterLocation(loc);
			if (EnvironmentUtils.isSolid(loc.getBlock()) || MovingRock.isBlocked(hitbox)
					|| EarthLine.isBlocked(hitbox)) {
				toRemove.add(entity);
				continue;
			}
			entity.setLocation(loc.getX(), loc.getY(), loc.getZ(), 0, 0);

			hitboxes.add(hitbox);
		}
		this.location.add(this.velocity);
		this.hitbox.setCenterLocation(this.location);

		if (this.blocks.isEmpty() || this.location.distanceSquared(this.source) > maxRange2) {
			destroy();
		} else {
			List<Packet<?>> destroyPackets = new ArrayList<>();
			for (Entity entity : toRemove) {
				this.blocks.remove(entity);
				destroyPackets.add(new PacketPlayOutEntityDestroy(entity.getId()));
			}

			CraftMethods.sendPacket(Bending.getPlugin(), this.location.getWorld(), destroyPackets);

			BendingSoundManager.playSound(this.location, BendingSounds.WAVE_MOVE);

			sendPackets();

			if (this.growing && this.location.distanceSquared(this.source) >= growDistance2) {
				this.growing = false;
				Set<Entity> entities = this.blocks.keySet();
				for (Entity entity : entities) {
					this.blocks.put(entity, this.velocity);
				}
			}

			for (org.bukkit.entity.Entity entity : EnvironmentUtils.getEntitiesInHitbox(this.hitbox)) {
				Hitbox box;
				if (entity instanceof LivingEntity) {
					box = new Hitbox((LivingEntity) entity);
				} else {
					box = new Hitbox(.5, .5);
					box.setBottomLocation(entity.getLocation());
				}

				for (Hitbox b : hitboxes) {
					if (box.isInside(b)) {
						Vector v = entity.getVelocity();
						MathUtils.setComponent(v, this.push);
						entity.setVelocity(v);
						MainThread.causeFlight(this.player.getPlayer(), entity);
						// this.location.getWorld().playSound(this.location,
						// Sound.SPLASH, 1F,
						// MathUtils.random.nextFloat() * .3F + 1.3F);
						break;
					}
				}
			}

			for (AbilityProjectile projectile : AbilityProjectile.getProjectilesInHitbox(this.hitbox)) {
				if (projectile.getShooter().equals(this.player.getPlayer())) {
					continue;
				}
				for (Hitbox b : hitboxes) {
					if (b.isInside(projectile)) {
						projectile.kill();
						break;
					}
				}
			}
		}

	}

	private void sendPackets() {
		List<Packet<?>> packets = new ArrayList<>();
		for (Entity block : this.blocks.keySet()) {
			// packets.add(new PacketPlayOutSpawnEntity(block, 70, 8));
			packets.add(new PacketPlayOutSpawnEntity(block, 70, 79));
		}

		CraftMethods.sendPacket(Bending.getPlugin(), this.location.getWorld(), packets);
	}

}
