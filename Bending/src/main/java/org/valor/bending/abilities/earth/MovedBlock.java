package main.java.org.valor.bending.abilities.earth;

import org.bukkit.block.BlockState;

public class MovedBlock {

	public BlockState state;
	public long time;

	public MovedBlock(BlockState state, long time) {
		this.state = state;
		this.time = time;
	}

}
