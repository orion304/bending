package main.java.org.valor.bending.abilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum AbilityType {

	AIR_BLAST(Element.AIR), EARTH_BLAST(Element.EARTH), FIRE_BLAST(Element.FIRE), WATER_BLAST(
			Element.WATER), RAISE_EARTH(Element.EARTH), WATER_SPOUT(Element.WATER), WAVE(Element.WATER), EARTH_LINE(
					Element.EARTH), AIR_SUCTION(Element.AIR), AIR_SHIELD(Element.AIR), TORNADO(Element.AIR), AIR_SPOUT(
							Element.AIR), MOBILITY(Element.AIR), AIR_BURST(Element.AIR), AIR_SWIPE(Element.AIR);

	private static Map<Element, List<AbilityType>> abilities = new HashMap<>();

	static {
		for (Element element : Element.values()) {
			abilities.put(element, new ArrayList<AbilityType>());
		}

		for (AbilityType ability : values()) {
			abilities.get(ability.element).add(ability);
		}
	}

	public static List<AbilityType> getAbilities(Element element) {
		return abilities.get(element);
	}

	public final Element element;

	private final String permission;

	private final String name;

	private AbilityType(Element element) {
		this.element = element;

		this.permission = element.getPermissionNode() + "." + name().toLowerCase().replaceAll("_", "");

		StringBuilder builder = new StringBuilder();
		builder.append(element.color);
		String[] bigSplit = name().split("__");
		for (int i = 0; i < bigSplit.length; i++) {
			String[] smallSplit = bigSplit[i].split("_");
			for (int j = 0; j < smallSplit.length; j++) {
				String string = smallSplit[j];
				char c = string.charAt(0);
				String rest = string.substring(1);
				builder.append(c);
				builder.append(rest.toLowerCase());
				if (j != smallSplit.length - 1) {
					builder.append(" ");
				}
			}
			if (i != bigSplit.length - 1) {
				builder.append(" / ");
			}
		}
		this.name = builder.toString();
	}

	public String getPermissionNode() {
		return this.permission;
	}

	@Override
	public String toString() {
		return this.name;
	}

}
