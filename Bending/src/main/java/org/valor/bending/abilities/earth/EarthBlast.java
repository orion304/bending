package main.java.org.valor.bending.abilities.earth;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

import main.java.org.valor.bending.abilities.AbilityProjectile;
import main.java.org.valor.bending.abilities.AbilityType;
import main.java.org.valor.bending.abilities.fire.FireBlast;
import main.java.org.valor.bending.abilities.water.WaterBlast;
import main.java.org.valor.bending.main.Bending;
import main.java.org.valor.bending.main.BendingPlayer;
import main.java.org.valor.bending.sounds.BendingSoundManager;
import main.java.org.valor.bending.sounds.BendingSounds;
import net.minecraft.server.v1_12_R1.Entity;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_12_R1.PacketPlayOutSpawnEntity;
import src.main.java.org.orion304.utils.CraftMethods;
import src.main.java.org.orion304.utils.MathUtils;
import src.main.java.org.orion304.utils.RelationshipUtils;

public class EarthBlast extends AbilityProjectile {

	private static final double speed = 1.5;

	private Entity block = null;
	private final int packetData;

	public EarthBlast(BendingPlayer source, FloatingRock block) {
		super(AbilityType.EARTH_BLAST, block.getLocation().clone(), new Vector(0, 1, 0), source.getPlayer(), false,
				true, true, .5D);
		block.kill();
		BendingSoundManager.playSound(this.location, BendingSounds.EARTH_BLAST_THROW);
		source.earthSource = null;
		LivingEntity target = RelationshipUtils.getTargetedEntity(this.shooter, 50, LivingEntity.class);
		Location destination;
		if (target == null) {
			destination = RelationshipUtils.getTargetedLocation(this.shooter, 50);
		} else {
			destination = target.getLocation().add(0, 1, 0);
		}
		this.velocity = MathUtils.getDistanceVector(this.location, destination).normalize().multiply(speed);
		this.block = CraftMethods.getNewEntity(this.location);
		this.packetData = block.packetData;
	}

	@Override
	protected void animate() {
		for (BendingPlayer player : Bending.getPlugin().handler.getCustomPlayers()) {
			if (player.earthSource != null && player.earthSource.hitbox.isInside(this.hitbox)
					&& Bending.isBendable(this.shooter, player.earthSource.getLocation(), this.type)) {
				player.earthSource.kill();
				player.earthSource = null;
				this.isAlive = false;
				return;
			}
		}

		if (MovingRock.isBlocked(this.hitbox)) {
			this.isAlive = false;
			return;
		}

		for (AbilityProjectile projectile : AbilityProjectile.getProjectilesInHitbox(this.hitbox)) {
			if (!Bending.isBendable(this.shooter, projectile.getLocation(), this.type)) {
				continue;
			}
			if (projectile instanceof WaterBlast || (projectile instanceof EarthBlast && !projectile.equals(this))) {
				projectile.setAlive(false);
				this.isAlive = false;
				return;
			} else if (projectile instanceof FireBlast) {
				projectile.setAlive(false);
			}
		}

		spawn();
	}

	@Override
	public void run() {
		super.run();
		if (!this.isAlive && this.block != null) {
			CraftMethods.sendPacket(this.plugin, this.location.getWorld(),
					new PacketPlayOutEntityDestroy(this.block.getId()));
			BendingSoundManager.playSound(this.location, BendingSounds.EARTH_BLAST_BREAK);
		}
	}

	private void spawn() {
		this.block.setLocation(this.location.getX(), this.location.getY(), this.location.getZ(), 0, 0);

		CraftMethods.sendPacket(this.plugin, this.location.getWorld(),
				new PacketPlayOutSpawnEntity(this.block, 70, this.packetData));
	}

}
