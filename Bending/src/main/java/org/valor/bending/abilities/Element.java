package main.java.org.valor.bending.abilities;

import org.bukkit.ChatColor;

public enum Element {

	WATER(ChatColor.AQUA), EARTH(ChatColor.GREEN), FIRE(ChatColor.RED), AIR(
			ChatColor.GRAY), MARTIAL(ChatColor.GOLD);

	public final ChatColor color;
	private final String permission;
	private final String name;

	private Element(ChatColor color) {
		this.permission = "bending." + name().toLowerCase();
		this.color = color;
		this.name = color + name().substring(0, 1)
				+ name().substring(1).toLowerCase();
	}

	public String getPermissionNode() {
		return this.permission;
	}

	@Override
	public String toString() {
		return this.name;
	}

}
