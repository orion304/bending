package main.java.org.valor.bending.abilities.water;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Player;

import main.java.org.valor.bending.main.BendingTools;

public class Ice {

	private static final double range = 30, range2 = range * range;

	private static Map<Block, Ice> instances = new HashMap<>();

	public static void destroy(Block block) {
		Ice ice = instances.get(block);
		if (ice != null) {
			ice.destroy();
		}
	}

	public static void destroyAll() {
		List<Ice> ices = new ArrayList<>();
		ices.addAll(instances.values());
		for (Ice ice : ices) {
			ice.destroy();
		}
	}

	public static void handle() {
		List<Block> blocks = new ArrayList<>();
		blocks.addAll(instances.keySet());
		for (Block block : blocks) {
			Ice ice = instances.get(block);
			ice.progress();
		}
	}

	public static boolean isIce(Block block) {
		return instances.containsKey(block);
	}

	public static boolean isSource(Block block, Player player) {
		Ice ice = instances.get(block);
		if (ice != null) {
			return BendingTools.isWaterbendable(ice.state.getType(), player);
		}
		return true;
	}

	private final Block block;
	private final BlockState state;

	private final Player player;

	public Ice(Block block, Player player) {
		this.block = block;
		this.player = player;

		if (instances.containsKey(block)) {
			Ice ice = instances.get(block);
			this.state = ice.state;
		} else {
			this.state = block.getState();
		}
		block.setType(Material.ICE);

		instances.put(block, this);
	}

	public void destroy() {
		this.state.update(true, true);
		instances.remove(this.block);
	}

	private void progress() {
		Location location = this.player.getLocation();
		Location loc = this.block.getLocation();
		if (!this.player.isOnline() || this.player.isDead()
				|| location.getWorld() != loc.getWorld()
				|| loc.distanceSquared(location) > range2) {
			destroy();
		}
	}

}
