package main.java.org.valor.bending.abilities.air;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import main.java.org.valor.bending.abilities.AbilityType;
import main.java.org.valor.bending.abilities.Progression;
import main.java.org.valor.bending.main.Bending;
import main.java.org.valor.bending.main.BendingPlayer;
import main.java.org.valor.bending.main.MainThread;
import net.minecraft.server.v1_12_R1.EnumParticle;
import src.main.java.org.orion304.utils.CraftMethods;
import src.main.java.org.orion304.utils.MathUtils;

public class Mobility extends Progression {

	private static final Vector up = new Vector(0, 1, 0);
	private static final double speed = 2;
	private static final double maxGlideSpeed = .6;
	private static final double maxY = -.2;

	private Vector velocity = null;
	private BendingPlayer player;

	public Mobility(BendingPlayer player, boolean shift) {
		this.player = player;
		Player entity = player.getPlayer();
		Location location = entity.getLocation();
		if (!Bending.isBendable(entity, location, AbilityType.MOBILITY)) {
			return;
		}

		if (shift) {
			this.velocity = player.getVelocity().clone();
			this.isAlive = true;
		} else {
			Vector velocity = player.getVelocity();
			velocity.setY(0);
			final Vector front = entity.getEyeLocation().getDirection().clone();
			front.setY(0);
			if (front.lengthSquared() == 0) {
				return;
			}
			front.normalize().multiply(speed);
			front.setY(.4);
			if (velocity.lengthSquared() == 0) {
				velocity = front.clone();
			}
			velocity.setY(0);
			final Vector left = MathUtils.rotateVectorAroundVector(up, front,
					90);
			final Vector right = MathUtils.rotateVectorAroundVector(up, front,
					-90);
			final Vector back = MathUtils.rotateVectorAroundVector(up, front,
					180);
			final double threshold = Math.toRadians(30);
			if (velocity.angle(front) <= threshold) {
				Vector direction = entity.getEyeLocation().getDirection()
						.normalize().multiply(speed);
				if (direction.getY() < .4) {
					direction.setY(.4);
				}
				entity.setVelocity(direction);
				CraftMethods.sendPacket(Bending.getPlugin(), entity.getWorld(),
						CraftMethods.getParticlePacket(EnumParticle.CLOUD,
								true, location, 0, 0, 0, 0, 1));
			} else if (velocity.angle(back) <= threshold) {
				entity.setVelocity(back);
			} else if (velocity.angle(left) <= Math.toRadians(90) - threshold) {
				entity.setVelocity(left);
			} else {
				entity.setVelocity(right);
			}

			MainThread.causeFlight(entity, entity);
			this.isAlive = false;
		}

	}

	@Override
	protected void progress() {
		if (!this.player.getPlayer().isSneaking()
				|| this.player.getAbility() != AbilityType.MOBILITY
				|| ((LivingEntity) this.player.getPlayer()).isOnGround()) {
			kill();
			return;
		}

		if (this.velocity.getY() >= 0) {
			this.velocity = this.player.getVelocity().clone();
		}

		double y = this.velocity.getY();
		if (y < 0) {
			double x = this.velocity.getX();
			double z = this.velocity.getZ();
			if (x * x + z * z > maxGlideSpeed) {
				this.velocity.setX(.98 * x);
				this.velocity.setZ(.98 * z);
			}
			if (y > maxY) {
				y -= .02;
				this.velocity.setY(y);
			}

			if (y < maxY) {
				y = maxY;
				this.velocity.setY(y);
			}

			this.player.getPlayer().setVelocity(this.velocity);
		}
	}

}
