package main.java.org.valor.bending.abilities.earth;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

import main.java.org.valor.bending.abilities.AbilityType;
import main.java.org.valor.bending.abilities.Progression;
import main.java.org.valor.bending.main.Bending;
import main.java.org.valor.bending.main.BendingTools;
import main.java.org.valor.bending.sounds.BendingSoundManager;
import main.java.org.valor.bending.sounds.BendingSounds;
import src.main.java.org.orion304.utils.EnvironmentUtils;

public class EarthColumn extends Progression {

	private static final Vector up = new Vector(0, 1, 0);

	public static final int maxlength = 5;

	private List<MovingRock> rocks = new ArrayList<>();

	public EarthColumn(LivingEntity bender) {
		this(bender, BendingTools.getEarthSourceBlock(bender, 20), up, maxlength, false);
	}

	public EarthColumn(LivingEntity player, Block source, Vector direction, int maxlength, boolean moveMovedBlocks) {
		super();
		Vector up = direction.clone().normalize();
		Vector down = up.clone().multiply(-1);
		Location location = source.getLocation().add(.5, .5, .5);
		for (int i = 1; i <= maxlength; i++) {
			location.add(up);
			Block above = location.getBlock();
			if (!Bending.isBendable(player, above.getLocation(), AbilityType.RAISE_EARTH)) {
				source = null;
				break;
			} else if (Bending.earthbendable.contains(above.getType())) {
				source = above;
			} else if (above.getType().isSolid()) {
				source = null;
				break;
			} else {
				location.subtract(up);
				break;
			}
		}

		Location check = location.clone();
		while (check.getBlock().equals(location.getBlock())) {
			check.add(up);
		}
		if (check.getBlock().getType().isSolid()) {
			source = null;
		}

		if (source == null) {
			return;
		}

		int length = BendingTools.getEarthbendableLength(player, source, down, maxlength, moveMovedBlocks);

		int height = length + 1;
		for (int i = 1; i <= length + 1; i++) {
			Block above = location.clone().add(up.clone().multiply(i)).getBlock();
			if (above.getType().isSolid()) {
				height = i - 1;
				break;
			}
		}

		for (int i = 0; i <= length; i++) {
			Block a = location.clone().add(down.clone().multiply((double) i)).getBlock();
			Block b = location.clone().add(up.clone().multiply((double) (height - i))).getBlock();
			if (!moveMovedBlocks && StoredBlocks.hasBeenMoved(a)) {
				continue;
			}
			MovingRock rock = new MovingRock(a, b, .5);
			if (rock.isAlive()) {
				this.isAlive = true;
			}
			this.rocks.add(rock);
		}
	}

	@Override
	protected void progress() {
		boolean alive = false;

		MovingRock first = this.rocks.get(0);

		Set<Entity> entities = EnvironmentUtils.getEntitiesAroundPoint(first.getLocation(), maxlength * 2,
				Entity.class);
		if (first.isAlive()) {
			World world = first.getLocation().getWorld();
			BendingSoundManager.playSound(first.getLocation(), BendingSounds.EARTH_COLUMN_MOVE);
		}

		Vector v = null;
		Set<Entity> affectedEntities = new HashSet<Entity>();
		for (MovingRock rock : this.rocks) {
			if (rock.isAlive()) {
				alive = true;
				for (Entity entity : entities) {
					if (entity.getLocation().distanceSquared(rock.getLocation()) <= 2.25) {
						affectedEntities.add(entity);
						v = rock.getVelocity();
					}
				}
				entities.removeAll(affectedEntities);
			}
		}

		for (Entity entity : affectedEntities) {
			Vector velocity = entity.getVelocity();
			Vector vhat = v.clone().normalize();
			double d = velocity.dot(vhat);
			velocity.subtract(vhat.multiply(d));
			velocity.add(v);
			entity.setVelocity(velocity.clone().multiply(1.1));
		}

		if (!alive) {
			this.isAlive = false;
		}
	}

}
