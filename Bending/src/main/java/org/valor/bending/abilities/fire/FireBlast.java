package main.java.org.valor.bending.abilities.fire;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

import main.java.org.valor.bending.abilities.AbilityProjectile;
import main.java.org.valor.bending.abilities.AbilityType;
import main.java.org.valor.bending.abilities.Progression;
import main.java.org.valor.bending.abilities.air.AirShield;
import main.java.org.valor.bending.abilities.earth.MovingRock;
import main.java.org.valor.bending.main.Bending;
import main.java.org.valor.bending.sounds.BendingSoundManager;
import main.java.org.valor.bending.sounds.BendingSounds;
import net.minecraft.server.v1_12_R1.EnumParticle;
import net.minecraft.server.v1_12_R1.Packet;
import src.main.java.org.orion304.utils.CraftMethods;
import src.main.java.org.orion304.utils.MathUtils;

public class FireBlast extends AbilityProjectile {

	private static final int streams = 3;

	private double angle = 0;
	private int i = 0;

	public FireBlast(final LivingEntity source) {
		this(source, source.getEyeLocation(), source.getEyeLocation().getDirection());
	}

	public FireBlast(final LivingEntity source, final Location location, final Vector velocity) {
		super(AbilityType.FIRE_BLAST, location, velocity.clone().normalize().multiply(3D), source, false, true, true,
				.3);
		// this.location.getWorld().playSound(location, Sound.STEP_WOOL, 1F,
		// .4F);
	}

	@Override
	public void animate() {
		for (AirShield shield : Progression.getByClass(AirShield.class)) {
			if (shield.isInShield(this.location)) {
				kill();
				return;
			}
		}
		if (this.location.getBlock().isLiquid()) {
			this.isAlive = false;
			return;
		}

		if (MovingRock.isBlocked(this.hitbox)) {
			this.isAlive = false;
			return;
		}

		for (AbilityProjectile projectile : AbilityProjectile.getProjectilesInHitbox(this.hitbox)) {
			if (!Bending.isBendable(this.shooter, projectile.getLocation(), this.type)) {
				continue;
			}
			if (projectile instanceof FireBlast && !projectile.equals(this)) {
				this.isAlive = false;
				projectile.setAlive(false);
				return;
			}
		}

		this.i++;
		this.angle += 35;

		Vector rotator = MathUtils.getOrthogonalVector(this.velocity, 0, .3);
		List<Packet<?>> packets = new ArrayList<>();
		for (int i = 0; i < streams; i++) {
			Vector v = MathUtils.rotateVectorAroundVector(this.velocity, rotator, this.angle + i * 360 / streams);
			Location loc = this.location.clone().add(v);
			packets.add(CraftMethods.getParticlePacket(EnumParticle.FLAME, true, loc, 0.2F, 0.2F, 0.2F, 0, 1));
		}
		packets.add(CraftMethods.getParticlePacket(EnumParticle.FLAME, true, this.location, .1F, .1F, .1F, 0, 1));
		CraftMethods.sendPacket(this.plugin, this.location.getWorld(), packets);
		if (this.i % 3 == 0) {
			BendingSoundManager.playSound(this.location, BendingSounds.FIRE_BLAST_MOVE);
		}

	}

	@Override
	public void run() {
		super.run();
		if (!this.isAlive) {
			BendingSoundManager.playSound(this.location, BendingSounds.FIRE_BLAST_HIT);
			CraftMethods.sendPacket(Bending.getPlugin(), this.location.getWorld(),
					CraftMethods.getParticlePacket(EnumParticle.FLAME, true, this.location, .75F, .75F, .75F, 0, 200));
		}
	}

}
