package main.java.org.valor.bending.abilities.air;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

import main.java.org.valor.bending.main.Bending;
import net.minecraft.server.v1_12_R1.EnumParticle;
import src.main.java.org.orion304.Hitbox;
import src.main.java.org.orion304.utils.CraftMethods;
import src.main.java.org.orion304.utils.MathUtils;

public class Etty {

	static final double dTheta = Math.toRadians(35);
	private static final double minRadius = .25;
	private static final double maxRadius = 3;
	private static final double startRadius = minRadius;
	static final double maxDistance = 20;
	private static final double maxDistanceSquared = maxDistance * maxDistance;

	private final Location location, origin;
	Vector velocity, ortho1, ortho2, push;
	private double radius = startRadius;
	private double theta;
	boolean isAlive = true;
	private final Hitbox hitbox;
	private final int I;
	private final double DR;

	public Etty(Location origin, Vector velocity, double theta, double scale) {
		this.origin = origin.clone();
		this.location = origin.clone();
		this.velocity = velocity.clone();
		this.I = (int) Math.ceil(this.velocity.length() / .5);
		this.DR = .1 / this.I;
		this.velocity.multiply(1 / (double) this.I);
		this.ortho1 = MathUtils.getOrthogonalVector(velocity, 0, 1);
		this.ortho2 = MathUtils.getOrthogonalVector(velocity, 90, 1);
		this.hitbox = new Hitbox(this.radius, this.radius);
		this.hitbox.setCenterLocation(this.location);
		this.theta = theta;
		this.push = velocity.clone().normalize().multiply(scale);
		if (this.push.getY() > .3) {
			this.push.setY(.3);
		}
	}

	private void animate() {
		int i;
		if (this.radius > maxRadius) {
			i = -1;
		} else if (this.radius < minRadius) {
			i = 1;
		} else {
			i = MathUtils.random.nextBoolean() ? 1 : -1;
		}

		double dr = MathUtils.random.nextDouble() * .1;// this.DR;
		this.radius += i * dr;
		this.theta += dTheta;

		Vector x = this.ortho1.clone().multiply(
				this.radius * Math.cos(this.theta));
		Vector y = this.ortho2.clone().multiply(
				this.radius * Math.sin(this.theta));
		x.add(y);

		Location loc = this.location.clone().add(x);
		CraftMethods.sendPacket(Bending.getPlugin(), this.location.getWorld(),
				CraftMethods.getParticlePacket(EnumParticle.SMOKE_NORMAL, true,
						loc, 0, 0, 0, 0, 1));
	}

	private boolean isInHitbox(Hitbox hitbox) {
		return hitbox.isInside(this.hitbox);
	}

	List<Entity> progress(Set<Entity> entities) {
		List<Entity> affected = new ArrayList<>();
		for (int i = 0; i < this.I; i++) {
			this.location.add(this.velocity);
			this.hitbox.setCenterLocation(this.location);
			this.hitbox.setHeight(this.radius);
			this.hitbox.setWidth(this.radius);
			if (this.location.distanceSquared(this.origin) > maxDistanceSquared
					|| this.location.getBlock().getType().isSolid()
					|| this.location.getBlock().isLiquid()) {
				this.isAlive = false;
				break;
			}
			for (Entity entity : entities) {
				Hitbox hitbox;
				if (entity instanceof LivingEntity) {
					hitbox = new Hitbox((LivingEntity) entity);
				} else {
					hitbox = new Hitbox(.5, .5);
					hitbox.setCenterLocation(entity.getLocation());
				}
				if (!affected.contains(entity) && isInHitbox(hitbox)) {
					affected.add(entity);
				}
			}
			animate();
		}
		return affected;
	}

}
