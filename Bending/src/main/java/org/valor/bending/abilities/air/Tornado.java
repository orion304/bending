package main.java.org.valor.bending.abilities.air;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.util.Vector;

import main.java.org.valor.bending.abilities.AbilityProjectile;
import main.java.org.valor.bending.abilities.AbilityType;
import main.java.org.valor.bending.abilities.Progression;
import main.java.org.valor.bending.abilities.fire.FireBlast;
import main.java.org.valor.bending.main.Bending;
import main.java.org.valor.bending.main.BendingPlayer;
import main.java.org.valor.bending.main.MainThread;
import net.minecraft.server.v1_12_R1.EnumParticle;
import net.minecraft.server.v1_12_R1.Packet;
import src.main.java.org.orion304.Hitbox;
import src.main.java.org.orion304.utils.CraftMethods;
import src.main.java.org.orion304.utils.EnvironmentUtils;
import src.main.java.org.orion304.utils.MathUtils;
import src.main.java.org.orion304.utils.RelationshipUtils;

public class Tornado extends Progression {

	private static final int streams = 7;
	private static final double maxHeight = 15;
	private static final double dHeight = .25;
	private static final double decreaseHeight = .1;
	private static final double tan = Math.tan(Math.toRadians(15));
	private static final double dTheta = Math.toRadians(30);

	private static final double speed = .05;

	private final BendingPlayer source;

	private Location location;
	private Vector velocity = null;
	private double height = 3;
	private final List<Double> radii = new ArrayList<>();
	private double theta = 0;
	private final Hitbox hitbox = new Hitbox(maxHeight, maxHeight);

	public Tornado(BendingPlayer source) {
		this.source = source;
		for (int i = 0; i < streams; i++) {
			this.radii.add(MathUtils.random.nextDouble() * .4 + .8);
		}
		this.isAlive = true;
	}

	public void direct() {
		Vector vector = this.source.getPlayer().getEyeLocation().getDirection();
		vector.setY(0);
		vector.normalize().multiply(.3);
		this.velocity = vector;
		if (this.location == null) {
			kill();
		}
	}

	private boolean isChanneling() {
		return this.source.getAbility() == AbilityType.TORNADO && this.source.getPlayer().isSneaking();
	}

	public boolean isDirected() {
		return this.velocity != null;
	}

	@Override
	protected void progress() {
		if (this.velocity == null) {
			if (!isChanneling() || this.source.getPlayer().isDead() || !this.source.getPlayer().isOnline()) {
				kill();
				return;
			}
		}

		if (this.velocity == null) {
			Block block = RelationshipUtils.getTargetedBlock(this.source.getPlayer(), 20,
					EnvironmentUtils.nonSolidNonLiquid);
			if (block == null) {
				return;
			} else {
				this.location = block.getLocation().add(.5, .5, .5);
			}
		} else {
			this.location.add(this.velocity);
		}

		Location loc;
		World world = this.location.getWorld();
		double y = this.location.getY();
		double dy = this.height / streams;
		this.hitbox.setBottomLocation(this.location.clone().add(0, 3 * dy, 0));
		y = y - dy;
		double h = 3 * dy;
		double t = this.theta;
		double x, z, r;
		double xi = this.location.getX();
		double zi = this.location.getZ();
		List<Packet<?>> packets = new ArrayList<>();
		for (int i = 0; i < streams; i++) {
			r = h * tan * this.radii.get(i);
			x = r * Math.cos(t) + xi;
			z = r * Math.sin(t) + zi;
			loc = new Location(world, x, y, z);
			if (Bending.isBendable(this.source.getPlayer(), loc, AbilityType.TORNADO)) {
				packets.add(CraftMethods.getParticlePacket(EnumParticle.SMOKE_NORMAL, true, loc, 0, 0, 0, 0, 1));
			}

			t += dTheta * 3;
			y += dy;
			h += dy;
		}
		CraftMethods.sendPacket(Bending.getPlugin(), world, packets);

		Vector v;
		for (Entity entity : EnvironmentUtils.getEntitiesAroundPoints(this.location, this.height)) {
			if (!Bending.isBendable(this.source.getPlayer(), entity.getLocation(), AbilityType.TORNADO)) {
				continue;
			}
			v = throwAt(entity.getLocation(), dy, !(entity instanceof Player));
			if (v != null && !entity.equals(this.source.getPlayer())) {
				entity.setVelocity(v);
				if (entity instanceof Projectile) {
					((Projectile) entity).setShooter(this.source.getPlayer());
				}
				entity.setFallDistance(0);
				MainThread.causeFlight(this.source.getPlayer(), entity);
			}
		}

		for (AbilityProjectile projectile : AbilityProjectile.getProjectilesInHitbox(this.hitbox)) {
			if (projectile.getShooter() == this.source.getPlayer()
					|| !Bending.isBendable(this.source.getPlayer(), projectile.getLocation(), AbilityType.TORNADO)) {
				continue;
			}
			v = throwAt(projectile.getLocation(), dy, true);
			if (v != null) {
				if (projectile instanceof AirBlast || projectile instanceof AirSuction) {
					projectile.kill();
				}
				if (projectile instanceof FireBlast) {
					projectile.setVelocity(v);
					projectile.setShooter(this.source.getPlayer());
				}
			}
		}

		if (this.velocity == null) {
			if (this.height < maxHeight) {
				this.height += dHeight;
				if (this.height > maxHeight) {
					this.height = maxHeight;
				}
			}
		} else {
			this.height -= decreaseHeight;
			if (this.height <= 0) {
				kill();
			}
		}

		this.theta += dTheta;
	}

	private Vector throwAt(Location location, double h, boolean up) {
		Vector distance = MathUtils.getDistanceVector(location, this.location);
		distance.setY(0);

		double y = location.getY();
		double yo = this.location.getY() - 3 * h;

		double dy = y - yo;

		if (dy < 0) {
			return null;
		}

		double r2 = distance.lengthSquared();
		double R = dy * tan;

		if (R * R < r2) {
			return null;
		}

		Vector v = new Vector(distance.getZ(), 0, -distance.getX());
		v.normalize().multiply(speed);
		v.add(distance.normalize().multiply(5 * speed));
		if (this.velocity != null) {
			v.add(this.velocity);
		}
		if (dy < 7) {
			v.setY(.3);
		}
		return v;

	}

}
