package main.java.org.valor.bending.abilities.air;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Projectile;
import org.bukkit.util.Vector;

import main.java.org.valor.bending.abilities.AbilityProjectile;
import main.java.org.valor.bending.abilities.AbilityType;
import main.java.org.valor.bending.abilities.Progression;
import main.java.org.valor.bending.main.Bending;
import net.minecraft.server.v1_12_R1.EnumParticle;
import src.main.java.org.orion304.Hitbox;
import src.main.java.org.orion304.utils.CraftMethods;
import src.main.java.org.orion304.utils.EnvironmentUtils;
import src.main.java.org.orion304.utils.MathUtils;

public class AirDeflect extends Progression {

	private static final long duration = 20L;
	private static final double radius = 1;
	private static final double dAngle = 40;
	private static final int streams = 3;
	private static final double dRadius = radius / streams;

	private long tick = 0;
	private final Vector normal;
	private final Location source;
	private final World world;
	private final Hitbox hitbox = new Hitbox(2 * radius, 2 * radius);
	private double angle;
	private final LivingEntity user;

	public AirDeflect(LivingEntity entity) {
		super();
		this.user = entity;
		this.source = entity.getEyeLocation().clone();
		this.normal = this.source.getDirection().normalize();
		this.source.add(this.normal.clone().multiply(1.5));
		this.hitbox.setCenterLocation(this.source);
		this.world = this.source.getWorld();

		if (!this.source.getBlock().isLiquid()) {
			this.isAlive = true;
		}
	}

	@Override
	protected void progress() {
		if (this.tick >= duration
				|| !Bending.isBendable(this.user, this.source,
						AbilityType.AIR_SHIELD)) {
			this.isAlive = false;
			return;
		}

		double radius = dRadius;
		Location loc = this.source.clone();
		for (int i = 1; i <= streams; i++) {
			CraftMethods.sendPacket(Bending.getPlugin(), this.world,
					CraftMethods.getParticlePacket(EnumParticle.SMOKE_NORMAL,
							true, loc, 0, 0, 0, 0, 1));
			loc = this.source.clone().add(
					MathUtils.getOrthogonalVector(this.normal, this.angle + i
							* dAngle * 2, radius));

			radius += dRadius;
		}

		for (AbilityProjectile projectile : AbilityProjectile
				.getProjectilesInHitbox(this.hitbox)) {
			if (projectile.getShooter() != this.user) {
				Vector velocity = projectile.getVelocity();
				double d = this.normal.dot(velocity);
				Vector v = velocity.subtract(this.normal.clone().multiply(
						2D * d));
				projectile.setVelocity(v);
				projectile.setShooter(this.user);
			}
		}

		for (Projectile projectile : EnvironmentUtils.getEntitiesAroundPoint(
				this.source, 2, Projectile.class)) {
			if (projectile.getShooter() != this.user) {
				Vector velocity = projectile.getVelocity();
				double d = this.normal.dot(velocity);
				Vector v = velocity.subtract(this.normal.clone().multiply(
						2D * d));
				projectile.setVelocity(v);
				projectile.setShooter(this.user);
			}
		}
		this.angle += dAngle;
		this.tick++;
	}

}
