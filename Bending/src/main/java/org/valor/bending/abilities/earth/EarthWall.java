package main.java.org.valor.bending.abilities.earth;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

import main.java.org.valor.bending.main.Bending;
import src.main.java.org.orion304.utils.MathUtils;

public class EarthWall {

	private static final Vector up = new Vector(0, 1, 0);
	private static final int width = 3;

	public EarthWall(LivingEntity entity, Block source) {
		Vector v = entity.getEyeLocation().getDirection();
		Location location = source.getLocation().add(.5, .5, .5);
		Vector side = MathUtils.rotateVectorAroundVector(up, v, 90).normalize();
		Location loc;
		Block block, b;
		for (int i = -width; i <= width; i++) {
			loc = location.clone().add(side.clone().multiply(i));
			block = loc.getBlock();
			for (int j = 0; j <= EarthColumn.maxlength; j++) {
				b = block.getRelative(BlockFace.DOWN, j);
				if (Bending.earthbendable.contains(b.getType())) {
					new EarthColumn(entity, b, up, EarthColumn.maxlength, false);
					break;
				} else if (b.getType().isSolid()) {
					break;
				}
			}
		}
	}

}
