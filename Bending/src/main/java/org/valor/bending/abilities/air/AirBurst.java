package main.java.org.valor.bending.abilities.air;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import main.java.org.valor.bending.abilities.AbilityType;
import main.java.org.valor.bending.abilities.Progression;
import main.java.org.valor.bending.main.Bending;
import main.java.org.valor.bending.main.BendingPlayer;
import main.java.org.valor.bending.main.MainThread;
import src.main.java.org.orion304.utils.EnvironmentUtils;
import src.main.java.org.orion304.utils.MathUtils;

public class AirBurst extends Progression {

	private static final double minAngle = 3 / Etty.maxDistance;

	private final BendingPlayer source;
	private boolean newEtties = true;
	private final List<Etty> etties = new ArrayList<>();
	private double scale = 1.5D;
	private double theta = 0;
	private Vector oldDirection = null;

	public AirBurst(BendingPlayer player) {
		this.source = player;
		this.isAlive = true;
	}

	private void affect(Entity entity, Etty etty) {
		Vector velocity = entity.getVelocity();
		MathUtils.setComponent(velocity, etty.push);
		entity.setVelocity(velocity);
		MainThread.causeFlight(this.source.getPlayer(), entity);
	}

	private boolean isChanneling() {
		Player player = this.source.getPlayer();
		return Bending.isBendable(player, player.getLocation(),
				AbilityType.AIR_BURST)
				&& player.isSneaking()
				&& this.source.getAbility() == AbilityType.AIR_BURST;
	}

	private void newEtty(Location location, Vector direction) {
		Location origin = location.clone().subtract(direction.clone());
		Etty etty = new Etty(location, direction, this.theta, this.scale);
		this.etties.add(etty);
		this.theta += Etty.dTheta * 2;
	}

	@Override
	protected void progress() {
		Player player = this.source.getPlayer();
		if (!isChanneling()) {
			this.newEtties = false;
		}

		Location location = player.getEyeLocation();
		location.subtract(0, .4, 0);
		if (this.newEtties && this.tick % 2 == 0) {
			Vector direction = location.getDirection().clone().normalize()
					.multiply(3D);
			if (this.oldDirection != null) {
				double angle = this.oldDirection.angle(direction);
				if (angle > minAngle) {
					int I = (int) Math.ceil(angle / minAngle);
					Vector ortho = this.oldDirection.clone().crossProduct(
							direction);
					ortho.normalize();
					double dTheta = Math.toDegrees(angle / I);
					for (int i = 0; i < I; i++) {
						// ServerUtils.verbose(this.oldDirection.length());
						this.oldDirection = MathUtils.rotateVectorAroundVector(
								ortho, this.oldDirection, dTheta);
						// ServerUtils.verbose(this.oldDirection.length());
						newEtty(location, this.oldDirection);
					}
				} else {
					newEtty(location, direction);
				}
			} else {
				newEtty(location, direction);
			}
			this.oldDirection = direction.clone();
			if (this.scale < 1) {
				this.scale += .05;
			}
		}

		Set<Entity> entities = EnvironmentUtils.getEntitiesAroundPoints(
				location, 50);
		List<Entity> affectedEntities = new ArrayList<>();

		List<Etty> list = new ArrayList<>();
		list.addAll(this.etties);
		for (Etty etty : list) {
			affectedEntities.addAll(etty.progress(entities));
			if (!etty.isAlive) {
				this.etties.remove(etty);
			} else {
				for (Entity entity : affectedEntities) {
					entities.remove(entity);
					if (entity.equals(player)) {
						continue;
					}
					affect(entity, etty);
				}
			}
			affectedEntities.clear();
		}

		if (this.etties.isEmpty() && !this.newEtties) {
			this.isAlive = false;
		}
	}

}
