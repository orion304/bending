package main.java.org.valor.bending.abilities.water;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import main.java.org.valor.bending.abilities.AbilityProjectile;
import main.java.org.valor.bending.abilities.AbilityType;
import main.java.org.valor.bending.abilities.Progression;
import main.java.org.valor.bending.abilities.air.AirSpout;
import main.java.org.valor.bending.main.Bending;
import main.java.org.valor.bending.sounds.BendingSoundManager;
import main.java.org.valor.bending.sounds.BendingSounds;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_12_R1.PacketPlayOutSpawnEntity;
import src.main.java.org.orion304.Hitbox;
import src.main.java.org.orion304.utils.CraftMethods;

public class WaterSpout extends Progression {

	private static final Vector up = new Vector(0, 1, 0);
	private static final Vector down = new Vector(0, -1, 0);

	private double maxHeight = 15;
	public Player player;
	private boolean couldFly;
	private List<net.minecraft.server.v1_12_R1.Entity> spout = new ArrayList<>();
	private List<WaterSpoutSpiral> spirals = new ArrayList<>();
	private float flySpeed;
	public Hitbox hitbox = new Hitbox(this.maxHeight, 1);

	private boolean fullyFormed = false;

	public WaterSpout(Player player) {
		super();
		this.player = player;
		for (AirSpout spout : Progression.getByClass(AirSpout.class)) {
			if (!spout.equals(this) && spout.player.equals(player)) {
				spout.kill();
				return;
			}
		}

		for (WaterSpout spout : Progression.getByClass(WaterSpout.class)) {
			if (!spout.equals(this) && spout.player.equals(player)) {
				spout.kill();
				return;
			}
		}

		double height = getSpoutableHeight();
		if (height >= 0) {
			this.couldFly = player.getAllowFlight();
			this.isAlive = true;
			spout(height);
		}
	}

	private double getSpoutableHeight() {
		Block block = this.player.getLocation().getBlock();
		for (int i = 0; i <= this.maxHeight + 5; i++) {
			Block b = block.getRelative(BlockFace.DOWN, i);
			if (Bending.isBendable(this.player, b.getLocation(), AbilityType.WATER_SPOUT)
					&& Bending.waterbendable.contains(b.getType())) {
				return b.getLocation().add(.5, .5, .5).distance(this.player.getLocation());
			} else if (b.getType().isSolid() && !(Bending.plantbendable.contains(b.getType())
					&& this.player.hasPermission("bending.water.plantbending"))) {
				return -1;
			}
		}
		return -1;
	}

	private double getYAnchor(Location location) {
		return Math.floor(location.getY() * 2D) / 2D;
	}

	@Override
	public void kill() {
		super.kill();
		List<Packet<?>> packets = new ArrayList<>();
		for (net.minecraft.server.v1_12_R1.Entity entity : this.spout) {
			packets.add(new PacketPlayOutEntityDestroy(entity.getId()));
		}
		for (WaterSpoutSpiral spiral : this.spirals) {
			packets.addAll(spiral.getDestroyPackets());
		}
		CraftMethods.sendPacket(Bending.getPlugin(), this.player.getWorld(), packets);
		this.player.setFlying(false);
		this.player.setAllowFlight(this.couldFly);
		this.player.setFallDistance(0F);
	}

	@Override
	protected void progress() {
		if (this.player.isDead() || !this.player.isOnline()) {
			kill();
			return;
		}
		double height = getSpoutableHeight();
		if (height == -1) {
			kill();
		} else {
			spout(height);
		}
	}

	private void spout(double height) {
		List<Packet<?>> packets = new ArrayList<>();

		Location location = this.player.getLocation();
		location.add(down.clone().multiply(height));

		double maxY = -1;
		if (!this.fullyFormed) {
			if (this.tick % 1 == 0) {
				WaterSpoutSpiral spiral;
				if (this.spirals.size() == 0) {
					double y = getYAnchor(location);
					spiral = new WaterSpoutSpiral(this.player, y, 0, y);
				} else {
					WaterSpoutSpiral previous = this.spirals.get(this.spirals.size() - 1);
					double previousY = previous.yAnchor;
					double currentY = previousY + .5D;
					double dy = previous.getLocation().getY() - previousY;
					spiral = new WaterSpoutSpiral(this.player, currentY + dy, previous.angle - WaterSpoutSpiral.dtheta,
							currentY);
				}
				this.spirals.add(spiral);
			}

			WaterSpoutSpiral first = this.spirals.get(this.spirals.size() - 1);
			double y = first.yAnchor;
			if (y >= this.player.getLocation().getY()) {
				this.fullyFormed = true;
			} else {
				maxY = y;
			}
		}

		if (this.fullyFormed) {
			WaterSpoutSpiral first = this.spirals.get(this.spirals.size() - 1);
			while (first.yAnchor < this.player.getLocation().getY()) {
				double previousY = first.yAnchor;
				double currentY = previousY + .5D;

				double dy = first.getLocation().getY() - previousY;
				first = new WaterSpoutSpiral(this.player, currentY + dy, first.angle - WaterSpoutSpiral.dtheta,
						currentY);
				this.spirals.add(first);
			}
		}

		boolean inSpout = false;
		boolean sound = this.tick % 5 == 0;
		List<WaterSpoutSpiral> toRemove = new ArrayList<>();
		for (int i = this.spirals.size() - 1; i > 0; i--) {
			WaterSpoutSpiral current = this.spirals.get(i);
			if (current.yAnchor >= this.player.getLocation().getY()) {
				toRemove.add(current);
				packets.addAll(current.getDestroyPackets());
			} else {
				WaterSpoutSpiral previous = this.spirals.get(i - 1);
				current.progress(previous.previousDy);
				packets.addAll(current.getSpawnPackets());
				if (sound && i % 4 == 0) {
					BendingSoundManager.playSound(current.getLocation(), BendingSounds.WATER_SPOUT_MOVE);
				}
			}
		}
		WaterSpoutSpiral first = this.spirals.get(0);
		// double dy = first.getLocation().getY() - first.yAnchor;
		// double vy = MathUtils.random.nextDouble() * .2D - .1D;
		// if (dy + vy > .5) {
		// vy = .5 - dy;
		// } else if (dy + vy < -.5) {
		// vy = .5 - dy;
		// }
		double vy = 0;
		first.progress(vy);
		packets.addAll(first.getSpawnPackets());
		this.spirals.removeAll(toRemove);

		int size;
		if (this.fullyFormed) {
			if (height <= this.maxHeight) {
				size = (int) Math.ceil(height * 2D);
			} else {
				size = (int) Math.ceil(this.maxHeight * 2D);
			}
		} else {
			size = (int) Math.ceil((maxY - location.getY()) * 2D);
		}

		if (this.fullyFormed && height <= this.maxHeight) {
			inSpout = true;
		}

		if (inSpout) {
			if (!this.player.getAllowFlight()) {
				this.player.setAllowFlight(true);
			}
			if (!this.player.isFlying()) {
				this.player.setFlying(true);
				Vector v = this.player.getVelocity();
				v.setY(0);
				this.player.setVelocity(v);
				this.flySpeed = this.player.getFlySpeed();
				this.player.setFlySpeed(0.05F);
			}
		} else {
			if (this.player.getAllowFlight()) {
				this.player.setAllowFlight(false);
			}

			if (this.player.isFlying()) {
				this.player.setFlying(false);
				this.player.setFlySpeed(this.flySpeed);
				this.player.setFallDistance(0F);
			}
		}

		net.minecraft.server.v1_12_R1.Entity entity;
		while (this.spout.size() > size) {
			int i = this.spout.size() - 1;
			entity = this.spout.get(i);
			CraftMethods.sendPacket(Bending.getPlugin(), this.player.getWorld(),
					new PacketPlayOutEntityDestroy(entity.getId()));
			this.spout.remove(i);
		}

		Vector v = up.clone().multiply(.5D);

		this.hitbox.setHeight(size * 2D);
		this.hitbox.setBottomLocation(location);

		for (AbilityProjectile projectile : AbilityProjectile.getProjectilesInHitbox(this.hitbox)) {
			if (projectile.getShooter() != this.player) {
				kill();
				return;
			}
		}

		for (int i = 0; i < size; i++) {
			if (this.spout.size() <= i) {
				entity = CraftMethods.getNewEntity(location);
				this.spout.add(entity);
			} else {
				entity = this.spout.get(i);
				entity.setLocation(location.getX(), location.getY(), location.getZ(), 0, 0);
			}
			// packets.add(new PacketPlayOutSpawnEntity(entity, 70, 8));
			packets.add(new PacketPlayOutSpawnEntity(entity, 70, 79));
			location.add(v);
		}

		CraftMethods.sendPacket(Bending.getPlugin(), this.player.getWorld(), packets);
	}

}
