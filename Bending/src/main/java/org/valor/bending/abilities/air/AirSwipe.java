package main.java.org.valor.bending.abilities.air;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

import main.java.org.valor.bending.abilities.AbilityProjectile;
import main.java.org.valor.bending.abilities.AbilityType;
import main.java.org.valor.bending.abilities.Progression;
import main.java.org.valor.bending.abilities.earth.MovingRock;
import main.java.org.valor.bending.abilities.water.WaterSpout;
import main.java.org.valor.bending.main.Bending;
import net.minecraft.server.v1_12_R1.EnumParticle;
import net.minecraft.server.v1_12_R1.Packet;
import src.main.java.org.orion304.Hitbox;
import src.main.java.org.orion304.utils.CraftMethods;
import src.main.java.org.orion304.utils.EnvironmentUtils;
import src.main.java.org.orion304.utils.MathUtils;

public class AirSwipe extends Progression {

	private static final double minAngle = 10;
	private static final double maxAngle = 60;
	private static final double minAngleR = Math.toRadians(minAngle);
	private static final double maxAngleR = Math.toRadians(maxAngle);
	private static final double speed = 2;
	private static final double maxDistance = 20;

	private final Vector firstVector;
	private Vector secondVector;
	private final LivingEntity source;
	private Vector velocity;
	private Location origin, location;
	private Vector ortho;
	private double angleR, angle;
	private final List<Double[]> blockedAngles = new ArrayList<>();
	private final Hitbox hitbox = new Hitbox(.5, .5);
	private final Set<Entity> nearbyEntities = new HashSet<>();

	public AirSwipe(LivingEntity entity) {
		this.source = entity;
		this.firstVector = this.source.getEyeLocation().getDirection().clone().normalize();
		this.isAlive = true;
	}

	private void affect(Entity entity, double r) {
		if (!entity.equals(this.source)
				&& Bending.isBendable(this.source, entity.getLocation(), AbilityType.AIR_SWIPE)) {
			Vector d = MathUtils.getDistanceVector(this.origin, entity.getLocation());
			double angle = this.firstVector.angle(d);
			for (Double[] blockedAngle : this.blockedAngles) {
				if (angle >= blockedAngle[0] && angle <= blockedAngle[1]) {
					this.nearbyEntities.remove(entity);
					return;
				}
			}

			if (entity instanceof LivingEntity) {
				((LivingEntity) entity).damage(3D, this.source);
				block(angle, r);
			} else {
				entity.setVelocity(MathUtils.getDistanceVector(this.origin, entity).normalize().multiply(speed));
			}

		}
	}

	private void block(double angle, double r) {
		double theta = 1 / (2D * r);
		double angle1 = angle - theta;
		double angle2 = angle + theta;
		List<Entity> keys = new ArrayList<>();
		keys.addAll(this.nearbyEntities);
		for (Entity entity : keys) {
			Vector d = MathUtils.getDistanceVector(this.origin, entity);
			double t = d.angle(this.firstVector);
			if (t >= angle1 && t <= angle2) {
				this.nearbyEntities.remove(entity);
			}
		}

		Double[] blocked = { Math.toDegrees(angle1), Math.toDegrees(angle2) };
		this.blockedAngles.add(blocked);
	}

	private void block(Location location, double r) {
		block(this.firstVector.angle(MathUtils.getDistanceVector(this.origin, location)), r);
	}

	@Override
	protected void progress() {
		if (this.tick == 10) {
			this.secondVector = this.source.getEyeLocation().getDirection().clone().normalize();
			this.origin = this.source.getEyeLocation().clone();
			this.location = this.origin.clone();

			if (this.firstVector.angle(this.secondVector) < minAngleR) {
				kill();
				return;
			}
			this.ortho = this.firstVector.clone().crossProduct(this.secondVector).normalize();
			if (this.firstVector.angle(this.secondVector) > maxAngleR || this.firstVector.dot(this.secondVector) < 0) {
				this.secondVector = MathUtils.rotateVectorAroundVector(this.ortho, this.firstVector, maxAngle);
			}
			this.angleR = this.firstVector.angle(this.secondVector);
			this.angle = Math.toDegrees(this.angleR);
			this.velocity = MathUtils.rotateVectorAroundVector(this.ortho, this.firstVector, this.angle / 2D)
					.normalize().multiply(speed / 4);
		}

		if (this.velocity != null) {
			this.nearbyEntities.clear();
			this.nearbyEntities
					.addAll(EnvironmentUtils.getEntitiesAroundPoint(this.location, maxDistance, LivingEntity.class));
			for (int j = 0; j < 4; j++) {
				this.location.add(this.velocity);
				double r = this.location.distance(this.origin);
				if (r > maxDistance) {
					kill();
					return;
				}
				double pathLength = Math.toRadians(this.angle) * r;
				int n = (int) Math.ceil(2D * pathLength);
				double dtheta = this.angle / n;
				Vector v = this.firstVector.clone();
				v.normalize().multiply(r);
				List<Packet<?>> packets = new ArrayList<>();
				Location loc = this.origin.clone().add(v);
				List<Entity> entities = new ArrayList<>();
				Hitbox h;
				double theta = 0;
				boolean goodAngle = true;
				for (int i = 0; i <= n; i++) {
					goodAngle = true;
					loc = this.origin.clone().add(v);
					v = MathUtils.rotateVectorAroundVector(this.ortho, v, dtheta);
					this.hitbox.setCenterLocation(loc);
					for (Double[] badAngles : this.blockedAngles) {
						if (theta >= badAngles[0] && theta <= badAngles[1]) {
							goodAngle = false;
							break;
						}
					}
					theta += dtheta;

					if (goodAngle) {
						for (AirShield shield : Progression.getByClass(AirShield.class)) {
							if (shield.isInShield(loc)) {
								block(theta, r);
								goodAngle = false;
								break;
							}
						}
					}

					if (goodAngle) {
						if (!EnvironmentUtils.nonSolidNonLiquid.contains(loc.getBlock().getType())
								|| !Bending.isBendable(this.source, loc, AbilityType.AIR_SWIPE)
								|| MovingRock.isBlocked(this.hitbox)) {
							block(loc.getBlock().getLocation().add(.5, .5, .5), r);
						} else {
							for (AbilityProjectile projectile : AbilityProjectile.getProjectilesInHitbox(this.hitbox)) {
								projectile.setAlive(false);
								block(loc, r);
								return;
							}

							entities.clear();
							entities.addAll(this.nearbyEntities);
							for (Entity entity : entities) {
								if (entity instanceof LivingEntity) {
									h = new Hitbox((LivingEntity) entity);
								} else {
									h = new Hitbox(.5, .5);
									h.setCenterLocation(entity.getLocation());
								}
								if (this.hitbox.isInside(h)) {
									affect(entity, r);
								}
							}

							for (AirSpout spout : Progression.getByClass(AirSpout.class)) {
								if (spout.isAlive && !spout.player.equals(this.source)
										&& spout.hitbox.isInside(this.hitbox)) {
									spout.kill();
								}
							}

							for (WaterSpout spout : Progression.getByClass(WaterSpout.class)) {
								if (spout.isAlive && !spout.player.equals(this.source)
										&& spout.hitbox.isInside(this.hitbox)) {
									spout.kill();
								}
							}

							entities.clear();
							if (j == 0) {
								packets.add(CraftMethods.getParticlePacket(EnumParticle.SMOKE_NORMAL, true, loc, 0, 0,
										0, 0, 1));
							}
						}
					}

				}
				CraftMethods.sendPacket(Bending.getPlugin(), this.location.getWorld(), packets);
			}
		}
	}

}
