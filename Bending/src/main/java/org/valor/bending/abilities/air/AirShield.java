package main.java.org.valor.bending.abilities.air;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

import main.java.org.valor.bending.abilities.AbilityProjectile;
import main.java.org.valor.bending.abilities.AbilityType;
import main.java.org.valor.bending.abilities.Progression;
import main.java.org.valor.bending.main.Bending;
import main.java.org.valor.bending.main.BendingPlayer;
import main.java.org.valor.bending.main.MainThread;
import net.minecraft.server.v1_12_R1.EnumParticle;
import src.main.java.org.orion304.Hitbox;
import src.main.java.org.orion304.utils.CraftMethods;
import src.main.java.org.orion304.utils.EnvironmentUtils;
import src.main.java.org.orion304.utils.MathUtils;

public class AirShield extends Progression {

	private static final Vector X = new Vector(1, 0, 0);
	private static final Vector Y = new Vector(0, 1, 0);
	private static final Vector Z = new Vector(0, 0, 1);
	private final double Xangle;
	private final double Yangle;
	private final double Zangle;

	private static final double maxRadius = 4;
	private static final double dR = .2;
	private static final double dTheta = 15;
	private static final double dT = dTheta * 4;
	private static final int streams = 7;
	private static final double dStream = 180D / (streams + 1);

	private final BendingPlayer source;
	private double angle = 0;
	private double radius = .5;
	private double radius2 = this.radius * this.radius;

	private Vector axis = new Vector(0, 1, 0);

	public final Hitbox hitbox = new Hitbox(maxRadius + 2, maxRadius + 2);

	public AirShield(BendingPlayer player) {
		super();

		this.source = player;
		this.hitbox.setCenterLocation(this.source.getPlayer().getLocation());
		this.Xangle = MathUtils.random.nextDouble() * 1.25 + .25;
		this.Yangle = MathUtils.random.nextDouble() * 1.25 + .25;
		this.Zangle = MathUtils.random.nextDouble() * 1.25 + .25;

		this.isAlive = true;
	}

	private boolean isChanneling() {
		return Bending.isBendable(this.source.getPlayer(), this.source
				.getPlayer().getLocation(), AbilityType.AIR_SHIELD)
				&& this.source.getAbility() == AbilityType.AIR_SHIELD
				&& this.source.getPlayer().isSneaking();
	}

	public boolean isInShield(Location location) {
		if (!location.getWorld().equals(this.source.getPlayer().getWorld())) {
			return false;
		}
		return location.distanceSquared(this.source.getPlayer().getLocation()) <= this.radius2;
	}

	@Override
	protected void progress() {
		if (!isChanneling()) {
			kill();
			return;
		}
		if (this.source.getPlayer().isDead()
				|| !this.source.getPlayer().isOnline()) {
			kill();
			return;
		}
		this.hitbox.setCenterLocation(this.source.getPlayer().getLocation());
		double theta = this.angle;
		Location playerLoc = this.source.getPlayer().getLocation();
		Location loc;
		World world = playerLoc.getWorld();
		for (int i = 1; i <= streams; i++) {
			Vector v = MathUtils.getOrthogonalVector(this.axis, theta,
					this.radius);
			Vector v1 = MathUtils.rotateVectorAroundVector(v, this.axis, i
					* dStream);

			loc = playerLoc.clone().add(v1.multiply(this.radius));

			if (Bending.isBendable(this.source.getPlayer(), loc,
					AbilityType.AIR_SHIELD)) {
				CraftMethods.sendPacket(Bending.getPlugin(), world,
						CraftMethods.getParticlePacket(
								EnumParticle.SMOKE_NORMAL, true, loc, 0, 0, 0,
								0, 1));
				// world.playSound(loc, Sound.HORSE_BREATHE, .5F, .4F);
			}

			theta = theta + dT;
		}

		for (Entity entity : EnvironmentUtils.getEntitiesAroundPoints(
				playerLoc, this.radius)) {
			Location eLoc = entity.getLocation();
			if (entity != this.source.getPlayer()
					&& !eLoc.getBlock().isLiquid()
					&& Bending.isBendable(this.source.getPlayer(),
							entity.getLocation(), AbilityType.AIR_SHIELD)
					&& eLoc.distanceSquared(playerLoc) > 1) {
				Vector v = MathUtils.getDistanceVector(playerLoc, eLoc);
				v.normalize().multiply(.3);
				double dx = eLoc.getX() - playerLoc.getX();
				double dz = eLoc.getZ() - playerLoc.getZ();
				entity.setVelocity(v.add(new Vector(-dz, 0, dx).normalize()
						.multiply(1)));
				MainThread.causeFlight(this.source.getPlayer(), entity);
			}
		}

		for (AbilityProjectile projectile : AbilityProjectile
				.getProjectilesInHitbox(this.hitbox)) {
			// if (!Bending.isBendable(this.source.getPlayer(),
			// projectile.getLocation(), AbilityType.AIR_SHIELD)) {
			// continue;
			// }
			// if ((projectile instanceof AirBlast
			// || projectile instanceof AirSuction
			// || projectile instanceof FireBlast || projectile instanceof
			// AirPunch)
			// && projectile.getLocation().distanceSquared(playerLoc) <=
			// this.radius
			// * this.radius) {
			// projectile.setAlive(false);
			// }
		}

		this.angle += dTheta;
		if (this.radius < maxRadius) {
			this.radius += dR;
			if (this.radius > maxRadius) {
				this.radius = maxRadius;
			}
			this.radius2 = this.radius * this.radius;
		}

		this.axis = MathUtils.rotateVectorAroundVector(X, this.axis,
				this.Xangle);
		this.axis = MathUtils.rotateVectorAroundVector(Y, this.axis,
				this.Yangle);
		this.axis = MathUtils.rotateVectorAroundVector(Z, this.axis,
				this.Zangle);

	}

}
