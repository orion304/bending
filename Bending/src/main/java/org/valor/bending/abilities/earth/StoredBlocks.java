package main.java.org.valor.bending.abilities.earth;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Player;

import main.java.org.valor.bending.main.BendingTools;
import main.java.org.valor.bending.sounds.BendingSoundManager;
import main.java.org.valor.bending.sounds.BendingSounds;
import src.main.java.org.orion304.utils.EnvironmentUtils;

public class StoredBlocks {

	private static long tick;
	private static final long regenTime = 20 * 60 * 5;

	private static Map<BlockState, Long> destroyedBlocks = new HashMap<>();
	private static Map<Block, MovedBlock> movedBlocks = new HashMap<>();

	public static void destroyBlock(Block block) {
		BlockState state;
		if (movedBlocks.containsKey(block)) {
			state = movedBlocks.get(block).state;
			movedBlocks.remove(block);
		} else {
			state = block.getState();
		}
		destroyedBlocks.put(state, tick + regenTime);
		BendingTools.clearBlock(block);
	}

	public static void handle(long tick) {
		StoredBlocks.tick = tick;
		if (tick % 100 == 0) {
			Set<Chunk> chunks = new HashSet<>();
			for (Player player : Bukkit.getOnlinePlayers()) {
				chunks.add(player.getLocation().getChunk());
			}

			List<Block> movedToRemove = new ArrayList<>();

			Set<Block> movedBlockKeys = new HashSet<>();
			movedBlockKeys.addAll(movedBlocks.keySet());
			for (Block block : movedBlockKeys) {
				if (!movedBlocks.containsKey(block)) {
					continue;
				}
				MovedBlock movedBlock = movedBlocks.get(block);
				if (movedBlock.time < tick
						&& (!movedBlock.state.getType().isSolid() || !chunks.contains(block.getChunk()))) {
					if (revert(movedBlock.state, false)) {
						BendingTools.clearBlock(block);
						movedToRemove.add(block);
					}
				}
			}

			for (Block block : movedToRemove) {
				movedBlocks.remove(block);
			}

			List<BlockState> destroyedToRemove = new ArrayList<>();

			Set<BlockState> destroyedBlockKeys = new HashSet<>();
			destroyedBlockKeys.addAll(destroyedBlocks.keySet());
			for (BlockState state : destroyedBlockKeys) {
				if (!destroyedBlocks.containsKey(state)) {
					continue;
				}
				long regenTick = destroyedBlocks.get(state);
				if (regenTick < tick && (!state.getType().isSolid() || !chunks.contains(state.getChunk()))) {
					if (revert(state, false)) {
						destroyedToRemove.add(state);
					}
				}
			}

			for (BlockState state : destroyedToRemove) {
				destroyedBlocks.remove(state);
			}

		}
	}

	public static boolean hasBeenMoved(Block block) {
		return movedBlocks.containsKey(block);
	}

	public static void moveBlock(BlockState from, Block to) {
		long time = tick + regenTime;
		MovedBlock movedBlock;
		if (movedBlocks.containsKey(from.getBlock())) {
			movedBlock = movedBlocks.get(from.getBlock());
			movedBlock.time = time;
			movedBlocks.remove(from.getBlock());
		} else {
			movedBlock = new MovedBlock(from, time);
		}

		if (movedBlock.state.getBlock().equals(to)) {
			movedBlocks.remove(movedBlock.state.getBlock());
			return;
		}

		movedBlocks.put(to, movedBlock);
	}

	public static void replaceAll() {
		Set<Block> movedBlockKeys = new HashSet<>();
		movedBlockKeys.addAll(movedBlocks.keySet());
		for (Block block : movedBlockKeys) {
			if (movedBlocks.containsKey(block)) {
				revert(movedBlocks.get(block).state, true);
				BendingTools.clearBlock(block);
			}
		}

		Set<BlockState> destroyedBlockKeys = new HashSet<>();
		destroyedBlockKeys.addAll(destroyedBlocks.keySet());
		for (BlockState state : destroyedBlockKeys) {
			if (destroyedBlocks.containsKey(state)) {
				revert(state, true);
			}
		}

		destroyedBlocks.clear();
		movedBlocks.clear();
	}

	public static boolean revert(BlockState state, boolean force) {
		Block block = state.getBlock();
		if (movedBlocks.containsKey(block)) {
			MovedBlock moved = movedBlocks.get(block);
			if (force || moved.time < tick + regenTime) {
				movedBlocks.remove(block);
				if (!revert(moved.state, force)) {
					return false;
				} else {
					BendingTools.clearBlock(block);
				}
			} else {
				return false;
			}
		}
		if (block.getType() == Material.AIR || EnvironmentUtils.isWater(block) || EnvironmentUtils.isLava(block)) {
			state.update(true, true);
		} else {
			BlockState nowState = block.getState();
			state.update(true, true);
			block.breakNaturally();
			nowState.update(true, true);
		}
		BendingSoundManager.playSound(block.getLocation().add(.5, .5, .5), BendingSounds.STORED_BLOCKS_REVERT);
		return true;
	}

}
