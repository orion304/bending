package main.java.org.valor.bending.abilities.earth;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;

import main.java.org.valor.bending.main.Bending;
import main.java.org.valor.bending.main.BendingTools;
import main.java.org.valor.bending.sounds.BendingSoundManager;
import main.java.org.valor.bending.sounds.BendingSounds;
import net.minecraft.server.v1_12_R1.Entity;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_12_R1.PacketPlayOutSpawnEntity;
import src.main.java.org.orion304.Hitbox;
import src.main.java.org.orion304.utils.CraftMethods;

public class FloatingRock {

	private static final double speed = .5;

	private MovingRock block = null;
	private final Entity entity;
	private Location location;
	public final int packetData;
	private BlockState state;
	private final Block source;
	public final Hitbox hitbox = new Hitbox(.5, .5);

	public FloatingRock(Block block) {
		int height = 0;
		for (int i = 1; i <= 2; i++) {
			if (!block.getRelative(BlockFace.UP, i).getType().isSolid()) {
				height = i;
			} else {
				break;
			}
		}
		this.source = block;
		if (block.getType() == Material.GRASS) {
			this.packetData = 3;
		} else {
			this.packetData = block.getTypeId() | (block.getData() << 0x10);
		}

		if (height > 0) {
			this.block = new MovingRock(block, block.getRelative(BlockFace.UP, height), speed, false);
			this.state = this.block.state;
			this.location = this.block.getLocation();
		} else {
			this.state = block.getState();
			this.location = block.getLocation().add(.5, .5, .5);
			BendingTools.clearBlock(block);
		}
		this.hitbox.setCenterLocation(this.location);

		BendingSoundManager.playSound(this.location, BendingSounds.FLOATING_ROCK_START);

		this.entity = CraftMethods.getNewEntity(this.location);
	}

	public void despawn() {
		if (this.block == null) {
			CraftMethods.sendPacket(Bending.getPlugin(), this.location.getWorld(),
					new PacketPlayOutEntityDestroy(this.entity.getId()));
		}
	}

	public void drop() {
		if (this.block == null || !this.block.isAlive()) {
			despawn();
			new MovingRock(this.state, this.location, this.source, speed, true);
		} else {
			this.block.setNewDestination(this.source.getLocation().add(.5, .5, .5));
			this.block.revert = true;
		}
	}

	public Location getLocation() {
		return this.location;
	}

	public void handle() {
		if (this.block != null) {
			this.location = this.block.getLocation();
			this.hitbox.setCenterLocation(this.location);
			this.entity.setLocation(this.location.getX(), this.location.getY(), this.location.getZ(), 0, 0);
			if (!this.block.isAlive()) {
				this.state = this.block.state;
				this.block = null;
			}
		}

		if (this.block == null) {
			CraftMethods.sendPacket(Bending.getPlugin(), this.location.getWorld(),
					new PacketPlayOutSpawnEntity(this.entity, 70, this.packetData));
		}

	}

	public void kill() {
		if (this.block == null) {
			despawn();
		} else {
			this.block.destroy();
		}
		BlockState currentState = this.state.getBlock().getState();
		this.state.update(true);
		StoredBlocks.destroyBlock(this.state.getBlock());
		currentState.update(true);
	}

}
