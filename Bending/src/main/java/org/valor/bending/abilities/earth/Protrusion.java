package main.java.org.valor.bending.abilities.earth;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.util.Vector;

import main.java.org.valor.bending.main.Bending;
import main.java.org.valor.bending.main.BendingTools;
import net.minecraft.server.v1_12_R1.Entity;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_12_R1.PacketPlayOutSpawnEntity;
import src.main.java.org.orion304.Hitbox;
import src.main.java.org.orion304.projectile.CustomProjectile;
import src.main.java.org.orion304.utils.CraftMethods;
import src.main.java.org.orion304.utils.EnvironmentUtils;
import src.main.java.org.orion304.utils.MathUtils;

public class Protrusion {

	private final Location source;
	private Location location;
	private final List<Entity> blockEntities = new ArrayList<>();
	private final List<Hitbox> hitboxes = new ArrayList<>();
	private double vx, vy, vz;
	private final double nx;
	private final double ny;
	private final double nz;
	private final Vector direction;
	private final int packetData;
	private final int maxLength;
	private final Hitbox hitbox;
	private boolean isReversed = false;
	public boolean isAlive = false;

	public Protrusion(Block block, Vector direction, int length) {
		this.source = block.getLocation().add(.5, .5, .5);
		this.location = this.source.clone();
		this.vx = direction.getX();
		this.vy = direction.getY();
		this.vz = direction.getZ();
		this.direction = direction.clone();
		Vector v = direction.clone().normalize().multiply(.5);
		this.nx = v.getX();
		this.ny = v.getY();
		this.nz = v.getZ();
		this.packetData = BendingTools.getPacketData(block);
		this.maxLength = length * length;
		this.hitbox = new Hitbox(length, 3);
		this.hitbox.setBottomLocation(this.source);
		this.isAlive = true;
		addBlock(this.source);
	}

	private void addBlock(Location location) {
		Entity entity = CraftMethods.getNewEntity(location);
		this.blockEntities.add(entity);
		Hitbox hitbox = new Hitbox(.5, .5);
		hitbox.setCenterLocation(this.source);
		this.hitboxes.add(hitbox);

	}

	private void addBlocks() {
		Location location = this.blockEntities.get(this.blockEntities.size() - 1).getBukkitEntity().getLocation()
				.clone();
		location.subtract(this.nx, this.ny, this.nz);
		if (location.distanceSquared(this.source) <= .25) {
			addBlock(location);
		} else {
			return;
		}
		addBlocks();
	}

	public Location getLocation() {
		return this.location;
	}

	public Vector getVelocity() {
		return new Vector(this.vx, this.vy, this.vz);
	}

	public boolean isInHitbox(CustomProjectile projectile) {
		if (!this.hitbox.isInside(projectile)) {
			return false;
		}

		for (Hitbox box : this.hitboxes) {
			if (box.isInside(projectile)) {
				return true;
			}
		}
		return false;
	}

	public boolean isInHitbox(Hitbox hitbox) {
		if (!this.hitbox.isInside(hitbox)) {
			return false;
		}

		for (Hitbox box : this.hitboxes) {
			if (hitbox.isInside(box)) {
				return true;
			}
		}
		return false;
	}

	public void kill() {
		this.isAlive = false;
		while (!this.blockEntities.isEmpty()) {
			removeBlock();
		}
	}

	protected void progress() {
		if (this.blockEntities.isEmpty()) {
			this.isAlive = false;
			return;
		}

		List<Packet<?>> packets = new ArrayList<>();
		for (int i = 0; i < this.blockEntities.size(); i++) {
			Entity block = this.blockEntities.get(i);
			block.locX += this.vx;
			block.locY += this.vy;
			block.locZ += this.vz;
			Location location = block.getBukkitEntity().getLocation();
			this.hitboxes.get(i).setCenterLocation(location);
			if (!EnvironmentUtils.isSolid(location.getBlock())) {
				packets.add(new PacketPlayOutSpawnEntity(block, 70, this.packetData));
			}
		}
		CraftMethods.sendPacket(Bending.getPlugin(), this.source.getWorld(), packets);

		Entity entity = this.blockEntities.get(this.isReversed ? this.blockEntities.size() - 1 : 0);
		org.bukkit.entity.Entity e = entity.getBukkitEntity();
		Location loc = e.getLocation();
		this.location = loc.clone();
		double d = loc.distanceSquared(this.source);
		if (this.isReversed) {
			removeBlocks();
		} else {
			addBlocks();
			if (d > this.maxLength) {
				reverse();
			}
		}

	}

	private void removeBlock() {
		if (this.blockEntities.isEmpty()) {
			return;
		}
		int i = this.blockEntities.size() - 1;
		Entity entity = this.blockEntities.get(i);
		this.hitboxes.remove(i);
		this.blockEntities.remove(i);
		CraftMethods.sendPacket(Bending.getPlugin(), this.source.getWorld(),
				new PacketPlayOutEntityDestroy(entity.getId()));
	}

	private void removeBlocks() {
		if (this.blockEntities.isEmpty()) {
			return;
		}
		int i = this.blockEntities.size() - 1;
		Entity entity = this.blockEntities.get(i);
		Vector v = MathUtils.getDistanceVector(this.source, entity.getBukkitEntity());
		if (v.dot(this.direction) <= 0) {
			removeBlock();
		} else {
			return;
		}
		removeBlocks();
	}

	private void reverse() {
		this.vx = -this.vx;
		this.vy = -this.vy;
		this.vz = -this.vz;
		this.isReversed = true;
	}

}
