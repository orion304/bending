package main.java.org.valor.bending.abilities.water;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import main.java.org.valor.bending.abilities.AbilityProjectile;
import main.java.org.valor.bending.abilities.AbilityType;
import main.java.org.valor.bending.abilities.earth.MovingRock;
import main.java.org.valor.bending.abilities.fire.FireBlast;
import main.java.org.valor.bending.main.Bending;
import main.java.org.valor.bending.main.BendingPlayer;
import main.java.org.valor.bending.main.BendingTools;
import main.java.org.valor.bending.sounds.BendingSoundManager;
import main.java.org.valor.bending.sounds.BendingSounds;
import net.minecraft.server.v1_12_R1.Entity;
import net.minecraft.server.v1_12_R1.EnumParticle;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_12_R1.PacketPlayOutSpawnEntity;
import src.main.java.org.orion304.utils.CraftMethods;
import src.main.java.org.orion304.utils.MathUtils;
import src.main.java.org.orion304.utils.RelationshipUtils;

public class WaterBlast extends AbilityProjectile {

	private static final int length = 5;
	private static final double prepspeed = .75;
	private static final double prepspeed2 = prepspeed * prepspeed;
	private static final double speed = 1.5;
	private static final double speed2 = speed * speed;

	private Location firstDestination, finalDestination;
	private final List<Entity> blocks = new ArrayList<>();
	private int i = 0;
	private boolean isIce;
	private boolean isRedirecting = false;
	private int redirectI;
	private Vector redirectVector;

	public WaterBlast(BendingPlayer source, boolean ice) {
		super(AbilityType.WATER_BLAST,
				source.waterSource == null ? source.getPlayer().getLocation()
						: source.waterSource.getLocation().clone(),
				new Vector(0, prepspeed, 0), source.getPlayer(), false, true, true, .5);
		if (source.waterSource == null) {
			this.isAlive = false;
			return;
		}
		// this.isIce = ice;
		this.isIce = true;
		this.location = source.waterSource.getLocation().add(.5, .5, .5);
		BendingTools.pullWaterFromSource(source.waterSource);
		if (this.isIce) {

		} else {
			BendingSoundManager.playSound(this.location, BendingSounds.WATER_BLAST_WATER_START);
		}
		LivingEntity target = RelationshipUtils.getTargetedEntity(source.getPlayer(), 50, LivingEntity.class);
		Location destination;
		if (target == null) {
			destination = RelationshipUtils.getTargetedLocation(source.getPlayer(), 50);
		} else {
			destination = target.getLocation().add(0, 1, 0);
		}

		this.finalDestination = destination;
		Vector v = MathUtils.getDistanceVector(this.location, this.finalDestination);
		v.setY(0);
		v.normalize();
		v.setY(2);
		this.firstDestination = this.location.clone().add(v);
		source.waterSource = null;
	}

	@Override
	protected void animate() {
		for (BendingPlayer player : Bending.getPlugin().handler.getCustomPlayers()) {
			if (player.earthSource != null && player.earthSource.hitbox.isInside(this.hitbox)
					&& Bending.isBendable(this.shooter, player.earthSource.getLocation(), this.type)) {
				player.earthSource.kill();
				player.earthSource = null;
				this.isAlive = false;
				return;
			}
		}

		if (MovingRock.isBlocked(this.hitbox)) {
			this.isAlive = false;
			return;
		}

		for (AbilityProjectile projectile : AbilityProjectile.getProjectilesInHitbox(this.hitbox)) {
			if (!Bending.isBendable(this.shooter, projectile.getLocation(), this.type)) {
				continue;
			}
			if (projectile instanceof FireBlast || projectile instanceof WaterBlast && !projectile.equals(this)) {
				projectile.setAlive(false);
				this.isAlive = false;
				return;
			}
		}

		this.i++;
		if (this.firstDestination != null) {
			Vector v = MathUtils.getDistanceVector(this.source, this.location);
			Vector x = MathUtils.getDistanceVector(this.source, this.firstDestination);
			x.setY(0);
			x.normalize();
			double X = v.dot(x);

			if (X > 1) {
				this.firstDestination = null;
				this.velocity = MathUtils.getDistanceVector(this.location, this.finalDestination).normalize()
						.multiply(speed);
			} else {
				double dydx = Math.PI * Math.cos(Math.PI * X / 2D);
				double vx2 = prepspeed2 / (1 + dydx * dydx);
				double vy2 = prepspeed2 - vx2;

				this.velocity = x.multiply(Math.sqrt(vx2));
				this.velocity.setY(Math.sqrt(vy2));
			}
		} else if (this.isRedirecting) {
			if (!((Player) this.shooter).isSneaking()) {
				kill();
			} else {
				Vector distance = MathUtils.getDistanceVector(this.shooter, this.location);
				distance.setY(0);
				if (this.redirectI == -1) {
					Vector sight = this.shooter.getEyeLocation().getDirection();
					sight.setY(0);
					if (distance.angle(sight) < Math.toRadians(10)) {
						this.isRedirecting = false;
						Location newDestination;
						LivingEntity target = RelationshipUtils.getTargetedEntity(this.shooter, 50, LivingEntity.class);
						if (target == null) {
							newDestination = RelationshipUtils.getTargetedLocation(this.shooter, 50);
						} else {
							newDestination = target.getLocation().clone().add(0, 1, 0);
						}
						this.finalDestination = newDestination;
						this.velocity = MathUtils.getDistanceVector(this.location, newDestination).normalize()
								.multiply(speed);
					}
				} else if (this.i > this.redirectI + 30) {
					this.redirectI = -1;
				}

				if (this.isRedirecting) {
					double x = distance.getX();
					double z = distance.getZ();
					double xprime = -z;
					double yprime = x;
					double xprime2 = xprime * xprime;
					double yprime2 = yprime * yprime;
					double thetadot2 = speed2 / (xprime2 + yprime2);
					double thetadot = Math.sqrt(thetadot2);
					double xdot = xprime * thetadot;
					double ydot = yprime * thetadot;
					this.velocity = new Vector(xdot, 0, ydot);
				}
			}
		}

		if (this.i % 2 == 0) {
			if (this.isIce) {
				BendingSoundManager.playSound(this.location, BendingSounds.WATER_BLAST_ICE_MOVE);
			} else {
				BendingSoundManager.playSound(this.location, BendingSounds.WATER_BLAST_WATER_MOVE);
			}
			if (this.blocks.size() == length) {
				Entity lastBlock = this.blocks.get(length - 1);
				lastBlock.setLocation(this.location.getX(), this.location.getY(), this.location.getZ(), 0, 0);
				this.blocks.remove(length - 1);
				this.blocks.add(0, lastBlock);
			} else {
				Entity newBlock = CraftMethods.getNewEntity(this.location);
				this.blocks.add(0, newBlock);
			}
		}

		List<Packet<?>> packets = new ArrayList<>();
		for (Entity block : this.blocks) {
			packets.add(new PacketPlayOutSpawnEntity(block, 70, this.isIce ? 79 : 8));
		}

		CraftMethods.sendPacket(this.plugin, this.location.getWorld(), packets);

	}

	public void redirect(Player newShooter) {
		this.shooter = newShooter;
		// this.isIce = false;
		this.isRedirecting = true;
		this.redirectVector = MathUtils.getDistanceVector(newShooter, this.location);
		this.redirectVector.setY(0);
		this.redirectI = this.i;
		this.finalDestination = null;
	}

	@Override
	public void run() {
		super.run();
		if (!this.isAlive) {
			List<Packet<?>> packets = new ArrayList<>();
			for (Entity item : this.blocks) {
				packets.add(new PacketPlayOutEntityDestroy(item.getId()));
			}
			if (this.blocks.size() > 0) {
				packets.add(
						CraftMethods.getParticlePacket(this.isIce ? EnumParticle.SNOWBALL : EnumParticle.WATER_SPLASH,
								true, this.location, .3F, .3F, .3F, 0, 100));
			}
			CraftMethods.sendPacket(this.plugin, this.location.getWorld(), packets);
			if (this.isIce) {
				BendingSoundManager.playSound(this.location, BendingSounds.WATER_BLAST_ICE_BREAK);
			} else {
				BendingSoundManager.playSound(this.location, BendingSounds.WATER_BLAST_WATER_BREAK);
			}
		}
	}

}
