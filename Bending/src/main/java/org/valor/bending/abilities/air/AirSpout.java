package main.java.org.valor.bending.abilities.air;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import main.java.org.valor.bending.abilities.AbilityProjectile;
import main.java.org.valor.bending.abilities.AbilityType;
import main.java.org.valor.bending.abilities.Progression;
import main.java.org.valor.bending.abilities.water.WaterSpout;
import main.java.org.valor.bending.main.Bending;
import net.minecraft.server.v1_12_R1.EnumParticle;
import net.minecraft.server.v1_12_R1.Packet;
import src.main.java.org.orion304.Hitbox;
import src.main.java.org.orion304.utils.CraftMethods;
import src.main.java.org.orion304.utils.EnvironmentUtils;
import src.main.java.org.orion304.utils.MathUtils;

public class AirSpout extends Progression {

	private static final Vector up = new Vector(0, 1, 0);
	private static final Vector down = new Vector(0, -1, 0);
	private static final double radius = 1;
	private static final double dAngle = Math.toRadians(15);

	private final double maxHeight = 15;
	public Player player;
	private boolean couldFly;
	private final List<Double> spirals = new ArrayList<>();
	private double height = 0;
	private float flySpeed;
	private double angle = 0;

	private boolean fullyFormed = false;
	public Hitbox hitbox = new Hitbox(this.maxHeight, radius);

	public AirSpout(Player player) {
		super();
		this.player = player;
		for (AirSpout spout : Progression.getByClass(AirSpout.class)) {
			if (!spout.equals(this) && spout.player.equals(player)) {
				spout.kill();
				return;
			}
		}

		for (WaterSpout spout : Progression.getByClass(WaterSpout.class)) {
			if (!spout.equals(this) && spout.player.equals(player)) {
				spout.kill();
				return;
			}
		}

		for (int i = 0; i <= this.maxHeight; i++) {
			this.spirals.add(radius * (MathUtils.random.nextDouble() * .4 + .8));
		}
		double height = getSpoutY();
		if (height >= 0) {
			this.couldFly = player.getAllowFlight();
			this.isAlive = true;
			spout(height);
		}
	}

	private double getSpoutY() {
		Block block = this.player.getLocation().getBlock();
		if (!EnvironmentUtils.nonSolidNonLiquid.contains(block.getType())) {
			return -1;
		}
		for (int i = 0; i <= this.maxHeight + 5; i++) {
			Block b = block.getRelative(BlockFace.DOWN, i);
			if (!EnvironmentUtils.nonSolidNonLiquid.contains(b.getType())
					&& Bending.isBendable(this.player, b.getLocation(), AbilityType.AIR_SPOUT)) {
				return b.getRelative(BlockFace.UP).getY() + .5D;
			}
		}
		return -1;
	}

	@Override
	public void kill() {
		super.kill();
		this.player.setFlying(false);
		this.player.setAllowFlight(this.couldFly);
		if (this.flySpeed != 0) {
			this.player.setFlySpeed(this.flySpeed);
		}
		this.player.setFallDistance(0F);
	}

	@Override
	protected void progress() {
		if (this.player.isDead() || !this.player.isOnline()) {
			kill();
			return;
		}
		double y = getSpoutY();
		if (y == -1) {
			kill();
		} else {
			spout(y);
		}
		this.tick++;
	}

	private void spout(double yo) {
		Location playerLoc = this.player.getLocation();

		if (!this.fullyFormed) {
			if (this.height < this.maxHeight) {
				this.height += .5;
				if (this.height > this.maxHeight) {
					this.height = this.maxHeight;
				}
			}
		}

		double h = playerLoc.getY() - yo;
		boolean inSpout = h <= (this.fullyFormed ? this.maxHeight : this.height);

		if (inSpout) {
			if (!this.fullyFormed) {
				this.fullyFormed = true;
			}

			if (!this.player.getAllowFlight()) {
				this.player.setAllowFlight(true);
			}
			if (!this.player.isFlying()) {
				this.player.setFlying(true);
				Vector v = this.player.getVelocity();
				v.setY(0);
				this.player.setVelocity(v);
				this.flySpeed = this.player.getFlySpeed();
				this.player.setFlySpeed(0.05F);
			}
		} else {
			if (this.player.getAllowFlight()) {
				this.player.setAllowFlight(false);
			}

			if (this.player.isFlying()) {
				this.player.setFlying(false);
				this.player.setFlySpeed(this.flySpeed);
				this.player.setFallDistance(0F);
			}
		}

		if (this.fullyFormed) {
			this.height = h < this.maxHeight ? h : this.maxHeight;
		}

		Location loc = playerLoc.clone();
		double xo = loc.getX();
		double zo = loc.getZ();
		loc.setY(yo);
		this.hitbox.setBottomLocation(loc);
		this.hitbox.setHeight(this.height);
		double theta = this.angle;
		double x, y, z;
		List<Packet<?>> packets = new ArrayList<>();
		for (int i = 0; i <= this.height; i++) {
			x = this.spirals.get(i) * Math.cos(theta) + xo;
			z = this.spirals.get(i) * Math.sin(theta) + zo;
			y = yo + i;
			loc.setX(x);
			loc.setY(y);
			loc.setZ(z);
			packets.add(CraftMethods.getParticlePacket(EnumParticle.SMOKE_NORMAL, true, loc, 0, 0, 0, 0, 1));
			theta += 4 * dAngle;
		}
		this.angle += dAngle;

		for (AbilityProjectile projectile : AbilityProjectile.getProjectilesInHitbox(this.hitbox)) {
			if (projectile.getShooter() != this.player) {
				kill();
				return;
			}
		}

		CraftMethods.sendPacket(Bending.getPlugin(), this.player.getWorld(), packets);
	}

}
