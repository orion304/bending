package main.java.org.valor.bending.abilities.air;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

import main.java.org.valor.bending.abilities.AbilityProjectile;
import main.java.org.valor.bending.abilities.AbilityType;
import main.java.org.valor.bending.abilities.Progression;
import main.java.org.valor.bending.abilities.earth.EarthBlast;
import main.java.org.valor.bending.abilities.earth.MovingRock;
import main.java.org.valor.bending.abilities.fire.FireBlast;
import main.java.org.valor.bending.abilities.water.WaterBlast;
import main.java.org.valor.bending.main.Bending;
import main.java.org.valor.bending.main.BendingTools;
import main.java.org.valor.bending.main.MainThread;
import main.java.org.valor.bending.sounds.BendingSoundManager;
import main.java.org.valor.bending.sounds.BendingSounds;
import net.minecraft.server.v1_12_R1.EnumParticle;
import net.minecraft.server.v1_12_R1.Packet;
import src.main.java.org.orion304.Hitbox;
import src.main.java.org.orion304.utils.CraftMethods;
import src.main.java.org.orion304.utils.EnvironmentUtils;
import src.main.java.org.orion304.utils.MathUtils;

public class AirBlast extends AbilityProjectile {

	private static final int streams = 3;
	static final double maxY = .6;

	private double angle = 0;
	private double width = .1;
	private int i = 0;

	public AirBlast(final LivingEntity source) {
		this(source, source.getEyeLocation(), source.getEyeLocation().getDirection());
	}

	public AirBlast(final LivingEntity source, final Location location, final Vector velocity) {
		super(AbilityType.AIR_BLAST, location, velocity.clone().normalize().multiply(3D), source, false, true, false,
				.1);
	}

	@Override
	public void animate() {
		for (AirShield shield : Progression.getByClass(AirShield.class)) {
			if (shield.isInShield(this.location)) {
				kill();
				return;
			}
		}
		this.i++;
		this.angle += 15;
		if (this.width < 1) {
			this.width += .01;
		}
		this.hitbox.setHeight(this.width);
		this.hitbox.setWidth(this.width);

		if (this.location.getBlock().isLiquid()) {
			this.isAlive = false;
			return;
		}

		if (MovingRock.isBlocked(this.hitbox)) {
			this.isAlive = false;
			return;
		}

		for (AbilityProjectile projectile : AbilityProjectile.getProjectilesInHitbox(this.hitbox)) {
			if (!Bending.isBendable(this.shooter, projectile.getLocation(), AbilityType.AIR_BLAST)) {
				continue;
			}
			if (projectile instanceof WaterBlast || projectile instanceof EarthBlast) {
				this.isAlive = false;
				return;
			} else if (projectile instanceof FireBlast) {
				Vector v = projectile.getVelocity();
				double d = v.length();
				v = v.clone().add(this.velocity);
				this.isAlive = false;
				if (v.lengthSquared() == 0) {
					projectile.setAlive(false);
					return;
				} else {
					v.normalize().multiply(d);
				}
				projectile.setVelocity(v);
				return;
			} else if (projectile instanceof AirSuction || projectile instanceof AirBlast && !this.equals(projectile)) {
				projectile.setAlive(false);
				this.isAlive = false;
				return;
			}
		}

		for (final Entity entity : this.nearbyEntities) {
			if (entity.equals(getShooter())) {
				continue;
			}

			if (!Bending.isBendable(this.shooter, entity.getLocation(), this.type)) {
				continue;
			}
			Hitbox box;
			if (entity instanceof LivingEntity) {
				box = new Hitbox((LivingEntity) entity);
			} else {
				box = new Hitbox(.5, .5);
				box.setBottomLocation(entity.getLocation());
			}

			if (this.hitbox.isInside(box)) {
				// final Vector v = this.velocity.clone().normalize();
				// final Vector vel = entity.getVelocity();
				// double velY = vel.getY();
				// double vY = v.getY();
				// if (velY > maxY) {
				// vY = 0;
				// } else if (velY + vY > maxY) {
				// vY = maxY - velY;
				// }
				// v.setY(vY / 3D);
				// final double d = v.clone().normalize().dot(vel);
				// vel.subtract(v.clone().multiply(d));
				// vel.add(v.multiply(3D));

				Vector vel = entity.getVelocity();
				Vector v = this.velocity.clone();
				if (v.getY() > maxY) {
					v.setY(maxY);
				}
				MathUtils.setComponent(vel, v);

				entity.setVelocity(vel);

				entity.setFireTicks(0);
				entity.setFallDistance(0);
				MainThread.causeFlight(this.shooter, entity);
			}
		}

		for (Block block : this.nearbyBlocks) {
			if (!this.hitbox.isInside(block) || !Bending.isBendable(this.shooter, block.getLocation(), this.type)) {
				continue;
			}
			if (block.getType() == Material.FIRE) {
				BendingTools.clearBlock(block);
				BendingSoundManager.playSound(getLocation().add(.5, .5, .5), BendingSounds.AIR_BLAST_EXTINGUISH);
			} else if (EnvironmentUtils.isLava(block)) {
				block.setType(block.getData() == 0x0 ? Material.OBSIDIAN : Material.COBBLESTONE);
				BendingSoundManager.playSound(getLocation().add(.5, .5, .5), BendingSounds.AIR_BLAST_EXTINGUISH);
			}
		}

		Vector rotator = MathUtils.getOrthogonalVector(this.velocity, 0, this.width);
		List<Packet<?>> packets = new ArrayList<>();
		for (int i = 0; i < streams; i++) {
			Vector v = MathUtils.rotateVectorAroundVector(this.velocity, rotator, this.angle + i * 360 / streams);
			Location loc = this.location.clone().add(v);
			packets.add(CraftMethods.getParticlePacket(EnumParticle.SMOKE_NORMAL, true, loc, 0F, 0F, 0F, 0, 1));
		}
		// packets.add(CraftMethods.getParticlePacket("crit", this.location, 0,
		// 0,
		// 0, 0, 1));
		CraftMethods.sendPacket(this.plugin, this.location.getWorld(), packets);
		if (this.i % 3 == 0) {
			BendingSoundManager.playSound(this.location, BendingSounds.AIR_BLAST_MOVE);

		}
	}
}
