package main.java.org.valor.bending.sounds;

import org.bukkit.Location;

public class BendingSoundManager {

	public static void playSound(Location location, BendingSounds sound) {
		switch (sound) {
		case AIR_BLAST_EXTINGUISH:
			// block.getWorld().playSound(block.getLocation().add(.5, .5, .5),
			// Sound.FIZZ, .5F, 1F);
			break;
		case AIR_BLAST_MOVE:
			// this.location.getWorld().playSound(this.location,
			// Sound.HORSE_BREATHE, 1F, MathUtils.random.nextFloat() * .5F +
			// 1F);
			break;
		case EARTH_BLAST_BREAK:
			// this.location.getWorld().playSound(this.location, Sound.FALL_BIG,
			// 2F, .3F);
			break;
		case EARTH_BLAST_THROW:
			// this.location.getWorld().playSound(this.location,
			// Sound.IRONGOLEM_THROW, 1F, .5F);
			break;
		case EARTH_COLUMN_MOVE:
			// world.playSound(first.getLocation(), Sound.IRONGOLEM_THROW, 1F,
			// MathUtils.random.nextFloat() * .5F);
			// for (float f = .5F; f < .75F; f += .1F) {
			// world.playSound(first.getLocation(), Sound.STEP_GRAVEL, 1F, f);
			// }
			break;
		case FIRE_BLAST_HIT:
			// this.location.getWorld().playSound(this.location,
			// Sound.ZOMBIE_INFECT, 2F, .4F);
			break;
		case FIRE_BLAST_MOVE:
			// this.location.getWorld().playSound(this.location, Sound.FIRE, 1F,
			// MathUtils.random.nextFloat() * .4F + 1.6F);
			break;
		case FLOATING_ROCK_START:
			// for (float f = .5F; f < .75F; f += .1F) {
			// this.location.getWorld().playSound(this.location,
			// Sound.STEP_GRAVEL, 1F, f);
			// }
			break;
		case WATER_BLAST_ICE_BREAK:
			// this.location.getWorld().playSound(this.location, Sound.GLASS,
			// 2F, 1.5F);
			break;
		case WATER_BLAST_ICE_MOVE:
			// this.location.getWorld().playSound(this.location,
			// Sound.STEP_SNOW, 1F, 2F);
			break;
		case WATER_BLAST_WATER_BREAK:
			// this.location.getWorld().playSound(this.location, Sound.SPLASH,
			// 2F, 2F);
			break;
		case WATER_BLAST_WATER_MOVE:
			// this.location.getWorld().playSound(this.location, Sound.SWIM, 1F,
			// MathUtils.random.nextFloat() * .8F + 1.3F);
			break;
		case WATER_BLAST_WATER_START:
			// this.location.getWorld().playSound(this.location, Sound.SWIM, 1F,
			// .6F);
			break;
		case STORED_BLOCKS_REVERT:
			// block.getWorld().playSound(block.getLocation().add(.5, .5, .5),
			// Sound.ITEM_PICKUP, .2F, 1);
			break;
		case WATER_SPOUT_MOVE:
			// this.player.getWorld().playSound(current.getLocation(),
			// Sound.SWIM, .4F, MathUtils.random.nextFloat() * .4F + .5F);
			break;
		case WAVE_MOVE:
			// this.location.getWorld().playSound(this.location, Sound.SWIM, 1F,
			// MathUtils.random.nextFloat() * .3F + .8F);
			break;
		default:
			break;
		}
	}

	public static void playTestSound(Location location) {
		// CraftMethods.sendPacket(this, player.getWorld(), new
		// PacketPlayOutNamedSoundEffect(args[0], location.getX(),
		// location.getY(), location.getZ(), 1F, Float.parseFloat(args[1])));
	}

}
